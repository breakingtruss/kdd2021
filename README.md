Source code for the paper 'H. Chen, A. Conte, R. Grossi, G. Loukides, S. P. Pissis, and M. Sweering. On Breaking Truss-Based Communities' published at KDD 2021 Research Track.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 
COMPILATION AND EXAMPLE    
------------------------
The implementation of our approach that is used in experiments 
compiles and runs on a small example with ./compile.sh    

Before compiling, please install three libraries: igraph, Boost, and gflags. 

Links to these libraries are listed below:
	igraph: https://igraph.org/c/#downloads
	Boost: https://www.boost.org/users/download/
	gflags: https://github.com/gflags/gflags/blob/master/INSTALL.md

After installing the required libraries, please change the path '-I/home/igraph-0.8.3/include/ -L/home/igraph-0.8.3/lib'
in the compile.sh into your own installation path. 

Our approach was compiled with g++ version 7.3.0 using the following flags: -std=c++11 and -O3. 
We ran experiments on Scientific Linux 6.6.

INFORMATION ABOUT THE INPUT AND OUTPUT
---------------------------------------

1. Heuristics and exact algorithm 
    Input parameters (we refer to the parameters using the example in ./run.sh):

    k: This is the parameter k in the paper    

    dataset.txt: This is the input graph. The first line is the number of nodes in the graph (|V|). Each of the next |V| lines contains node id and node degree, for each node the input graph G. 
    Each of the next |E| lines contain the node ids of each edge lists of the input graph G.     
    
    For example, with input a 4-truss graph (in file simple.nde), the output is the following. We report the runtime of all methods, as well as the 
    number of edges they delete.  
    
 run: ./general_problem 3 < simple.nde     
----------------------Output for our method -----------------
 Sorting...     
 Cleaning...       
 Reading done     
 Permutation computed     
 Sorting again...     
 Permuting done     
 Number of edges = 11 , max_truss = 4     

------------------ ATk --------------------     
 ATk Runtime = 0 ms     
 ATk #. delete edge = 9     
------------------ GTk --------------------     
 GTk Runtime = 1 ms     
 GTk #. delete edge = 4    
------------------ MBH_s --------------------     
 MBH_s Runtime = 0 ms    
 MBH_s #. delete edge = 4    
------------------ MBH_c --------------------    
 MBH_c Runtime = 0 ms    
 MBH_c #. delete edge = 4    
------------------ SNH --------------------    
 SNH Runtime = 0 ms    
 SNH #. delete edge = 3    


run: ./findMinKtruss 3 <simple.nde       
----------------------Output for optimal solution -----------------     
Sorting...    
Cleaning...            
Reading done         
Permutation computed      
Sorting again...   
Permuting done   
number of edges = 11 , max_truss = 4    
Number of min 3 truss  = 8   
Time for find min Ktruss: 13 ms   
setfamily: minK3truss.dat ,#rows 8 ,#clms 11 ,#eles 0   
35   
0   
0   
0   
1   
14   
18   
2   
iters=37   
The min #. delete edge = 3    
#iterations= 37 ,#min_check= 99 ,#pruning= 12   
ave_cost_for_F(e)= 2.108108 ,ave_cost_for_CAND= 3.945946 ,ave_internal_sol.= 2.945946   
Total time taken : 0 s    

2. Lower bound      
The ECP.jar executes our lower bound algorithm (LB).     
It is bult on the top of the implementation of the pivot algorithm that was provided by the authors of paper 
'A. Conte, R. Grossi, and A. Marino. Large-scale clique cover of real-worldnetworks.Information and Computation, 270:104464, 2020'.    
The source code of LB is provided in ECP.zip.    

java -jar ECP.jar jazz.nde lower_bound    

3. Other information    
The folder shd31 contains code from the authors of paper 
'K. Murakami and T. Uno.  Efficient algorithms for dualizing large-scalehypergraphs. Discrete Applied Mathematics, 170:83 – 94, 2014' 
available at http://research.nii.ac.jp/~uno/code/shd31.zip

Comments and Questions    
----------------------   
Huiping Chen: huiping.chen@kcl.ac.uk   

Grigorios Loukides: grigorios.loukides@kcl.ac.uk   