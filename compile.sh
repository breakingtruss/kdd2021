

#!/bin/bash -e

if [ ! -f pkxsort.h ]
then
  wget https://raw.githubusercontent.com/voutcn/kxsort/778721d9b44bb942c0efbe9699379d4004c40298/kxsort.h -O pkxsort.h
  sha256sum -c sha256sums || exit 1
  patch pkxsort.h < pkxsort.h.patch
fi

CXX=clang++
CXXFLAGS="-O3 -I/home/igraph-0.8.3/include/ -L/home/igraph-0.8.3/lib -ligraph -fopenmp -Wall -march=native -lgflags"

${CXX} td_approx_external.cc ${CXXFLAGS} -o td_approx_external
${CXX} general_problem.cc ${CXXFLAGS} -o  general_problem
${CXX} findMinKtruss.cc ${CXXFLAGS} -o  findMinKtruss
