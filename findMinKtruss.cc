#include <gflags/gflags.h>
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

#include <map>
#include <string>
#include <iterator>
#include <vector> 
#include <set>
#include <stack> 
#include <unordered_set>
#include <unordered_map>

#include <chrono>  // NOLINT
#include <algorithm>
#include "common.h"           // NOLINT
#include "graph.h"            // NOLINT
#include "intersect_edges.h"  // NOLINT
#include "mmapped_vector.h"   // NOLINT

using namespace std;

DEFINE_string(filename, "", "Input filename. Omit for stdin.");
DEFINE_string(format, "nde",
              "Input file format. Valid values are nde, bin and sbin.");
DEFINE_string(storage_dir, "",
              "Directory for storage of working variables. Leave empty to use "
              "main memory.");

using edge_t = uint32_t;
using node_t = uint32_t;

using Graph = GraphT<node_t, edge_t>;

int **AllocateDynamicArray( long long nRows, int nCols){
      int **dynamicArray = new(nothrow) int*[nRows];
  
      for( long long i = 0 ; i < nRows ; i++ ){

      dynamicArray[i] = new(nothrow)  int [nCols]();
      }
      return dynamicArray;
}

void FreeDynamicArray(int** dArray, long long  nRows){
	for( long long i = 0 ; i < nRows ; i++ ){
      delete [] dArray[i] ; 
    }        
      delete [] dArray;
}


struct TrussDecompositionState {
  // Graph - node-sized vectors
  mmapped_vector<node_t> actual_degree;
  mmapped_vector<edge_t> adj_list_end;
  mmapped_vector<edge_t> adj_list_fwd_start;

  // Algorithm support structures (node-sized)
  mmapped_vector<node_t> dirty_nodes;
  mmapped_vector<uint8_t> dirty_nodes_bitmap;

  // Graph - edge-sized vectors
  mmapped_vector<edge_t> edge_id;
  mmapped_vector<int> trussness;

  // Algorithm support structures (edge-sized)
  mmapped_vector<node_t> edge1;
  mmapped_vector<node_t> edge2;
  mmapped_vector<uint32_t> edge_deleted;
  mmapped_vector<std::atomic<node_t>> edge_triangles_count;

  mmapped_vector<edge_t> edges_to_drop;
  edge_t to_drop_start = 0;
  std::atomic<edge_t> to_drop_end{0};
  edge_t iter_drop_start = 0;
  node_t min_size = 2;

  void SetStorageDir(const std::string &storage_dir) {
    actual_degree.init(FileFromStorageDir(storage_dir, "degrees.bin"));
    adj_list_end.init(FileFromStorageDir(storage_dir, "adj_list_end.bin"));
    adj_list_fwd_start.init(
        FileFromStorageDir(storage_dir, "adj_list_fwd_start.bin"));
    dirty_nodes.init(FileFromStorageDir(storage_dir, "dirty_nodes.bin"));
    dirty_nodes_bitmap.init(
        FileFromStorageDir(storage_dir, "dirty_nodes_bitmap.bin"));
    edge_id.init(FileFromStorageDir(storage_dir, "edge_id.bin"));
    trussness.init(FileFromStorageDir(storage_dir, "trussness.bin"));
    edge1.init(FileFromStorageDir(storage_dir, "edge1.bin"));
    edge2.init(FileFromStorageDir(storage_dir, "edge2.bin"));
    edge_deleted.init(FileFromStorageDir(storage_dir, "edge_deleted.bin"));
    edge_triangles_count.init(
        FileFromStorageDir(storage_dir, "edge_triangles_count.bin"));
    edges_to_drop.init(FileFromStorageDir(storage_dir, "edges_to_drop.bin"));
  }
};

#if defined(__AVX2__)
static __m256i avx2_zero = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 0, 0);
static __m256i avx2_one = _mm256_set_epi32(1, 1, 1, 1, 1, 1, 1, 1);
static __m256i bv_offsets_mask =
    _mm256_set_epi32(0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f);

__m256i ComputeCompress256Mask(unsigned int mask) {
  const uint32_t identity_indices = 0x76543210;
  uint32_t wanted_indices = _pext_u32(identity_indices, mask);
  __m256i fourbitvec = _mm256_set1_epi32(wanted_indices);
  __m256i shufmask = _mm256_srlv_epi32(
      fourbitvec, _mm256_set_epi32(28, 24, 20, 16, 12, 8, 4, 0));
  shufmask = _mm256_and_si256(shufmask, _mm256_set1_epi32(0x0F));
  return shufmask;
}
#endif

void printGraph(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s){
    size_t min_triangle = s->min_size;
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;

       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
          const node_t j = g->adj[cnt];
          auto edg = s->edge_id[cnt];
          size_t count =s->edge_triangles_count[edg].load(std::memory_order_relaxed);
          if(count < min_triangle) min_triangle = count;
          std::cout<<"edge id = "<<edg <<" : "<<i<<"->"<<j<<" support = "<<count<<" , trussness = "<<s->trussness[edg]<<std::endl;
       }
    }
   cout<<"s->min_size = "<<s->min_size<<endl;
}

// Re-compute the triangle count for an edge that has had adjacent edges
// removed.
void UpdateEdgeTriangles(Graph *__restrict__ g,
                         TrussDecompositionState *__restrict__ s, edge_t edg) {
  if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) == 0) return;
  node_t i = s->edge1[edg];
  node_t j = s->edge2[edg];
  edge_t inters = 0;
  IntersectEdges(
      g, g->adj_start[i], s->adj_list_end[i], g->adj_start[j],
      s->adj_list_end[j], [&](edge_t k, edge_t l) {
        edge_t edg2 = s->edge_id[k];
        edge_t edg3 = s->edge_id[l];
        bool edge2_deleted = s->edge_deleted[edg2 >> 5] & (1U << (edg2 & 0x1F));
        bool edge3_deleted = s->edge_deleted[edg3 >> 5] & (1U << (edg3 & 0x1F));

        if ((!edge2_deleted || edg2 > edg) && (!edge3_deleted || edg3 > edg)) {
          inters++;
          if (!edge2_deleted) {
            s->edge_triangles_count[edg2].fetch_sub(1,
                                                    std::memory_order_relaxed);
          }
          if (!edge3_deleted) {
            s->edge_triangles_count[edg3].fetch_sub(1,
                                                    std::memory_order_relaxed);
          }
        }
        return inters <
               s->edge_triangles_count[edg].load(std::memory_order_relaxed);
      });
}


void CountTrianglesFromScratch(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s) {                             
 
  s->edge_triangles_count.resize(g->adj_start.back() / 2);

  s->min_size =2;
#pragma omp parallel for schedule(guided)
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    thread_local std::vector<node_t> local_counts;
    local_counts.clear();
    local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      const node_t j = g->adj[cnt];
      IntersectEdges(g, cnt + 1, s->adj_list_end[i], s->adj_list_fwd_start[j],
                     s->adj_list_end[j], [&](edge_t k, edge_t l) {
                       edge_t edg3 = s->edge_id[l];
                       local_counts[cnt - s->adj_list_fwd_start[i]]++;
                       local_counts[k - s->adj_list_fwd_start[i]]++;
                       s->edge_triangles_count[edg3].fetch_add(
                           1, std::memory_order_relaxed);
                       return true;
                     });
    }
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      s->edge_triangles_count[s->edge_id[cnt]].fetch_add(
          local_counts[cnt - s->adj_list_fwd_start[i]],
          std::memory_order_relaxed);
    }
  }
  FullFence();
#pragma omp parallel for schedule(guided)
  for (node_t i = 0; i < g->N(); i++) {
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      auto edg = s->edge_id[cnt];
      if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) + 2 <
          s->min_size) {
        s->edges_to_drop[s->to_drop_end.fetch_add(
            1, std::memory_order_relaxed)] = edg;
      }
    }
  }
  FullFence();

}

// Remove gaps in the adjacency lists after removing edges.
void RecompactifyAdjacencyList(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s,
                               node_t i) {
  if (s->actual_degree[i] == 0) {
    s->adj_list_end[i] = s->adj_list_fwd_start[i] = g->adj_start[i];
    return;
  }
  s->adj_list_fwd_start[i] = g->adj_start[i];
  
  edge_t j = g->adj_start[i];
 
  node_t pos = g->adj_start[i];
#if defined(__AVX2__)
  __m256i avx2_i = _mm256_castps_si256(_mm256_broadcast_ss((float *)&i));
  for (; j + 7 < s->adj_list_end[i]; j += 8) {
    __m256i edge_ids = _mm256_loadu_si256((__m256i *)&s->edge_id[j]);
   
    __m256i vertices = _mm256_loadu_si256((__m256i *)&g->adj[j]);

    __m256i bv_offsets = _mm256_srai_epi32(edge_ids, 5);
    __m256i bv_shifts = _mm256_and_si256(edge_ids, bv_offsets_mask);

    __m256i deleted = _mm256_i32gather_epi32(
        (int *)s->edge_deleted.data(), bv_offsets, sizeof(s->edge_deleted[0]));
    deleted = _mm256_srav_epi32(deleted, bv_shifts);
    deleted = _mm256_and_si256(deleted, avx2_one);
    __m256i present_mask = _mm256_cmpeq_epi32(deleted, avx2_zero);

    __m256i greater_mask = _mm256_cmpgt_epi32(avx2_i, vertices);
    greater_mask = _mm256_and_si256(greater_mask, present_mask);

    uint32_t mask = _mm256_movemask_ps(_mm256_castsi256_ps(present_mask));

    if (!mask) continue;
    __m256i compress256_mask =
        ComputeCompress256Mask(_mm256_movemask_epi8(present_mask));
    edge_ids = _mm256_permutevar8x32_epi32(edge_ids, compress256_mask);
    vertices = _mm256_permutevar8x32_epi32(vertices, compress256_mask);

    _mm256_storeu_si256((__m256i *)&g->adj[pos], vertices);
    _mm256_storeu_si256((__m256i *)&s->edge_id[pos], edge_ids);
    pos += __builtin_popcount(mask);

    uint32_t greater_count = __builtin_popcount(
        _mm256_movemask_ps(_mm256_castsi256_ps(greater_mask)));
    s->adj_list_fwd_start[i] += greater_count;
  }
#endif

  for (; j < s->adj_list_end[i]; j++) {
    auto edg = s->edge_id[j];
    if (s->edge_deleted[edg >> 5] & (1U << (edg & 0x1F))) continue;
    if (g->adj[j] < i) s->adj_list_fwd_start[i]++;
    g->adj[pos] = g->adj[j];
    s->edge_id[pos] = s->edge_id[j];
    pos++;
  }
  s->adj_list_end[i] = pos;
}

// Update the number of triangles for each node and recompactify the adjacency
// list if needed.
void UpdateTriangleCounts(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s) {

#pragma omp parallel for schedule(dynamic)
  for (edge_t i = s->iter_drop_start; i < s->to_drop_start; i++) {
    edge_t edg = s->edges_to_drop[i];
    if (!(s->actual_degree[s->edge1[edg]] + 1 < s->min_size &&
          s->actual_degree[s->edge2[edg]] + 1 < s->min_size)) {
      UpdateEdgeTriangles(g, s, edg);
    }
  }
  FullFence();

  // Mark as to be removed edges whose triangle count became too low.
#pragma omp parallel for schedule(dynamic)
  for (node_t cnt = 0; cnt < s->dirty_nodes.size(); cnt++) {
    node_t i = s->dirty_nodes[cnt];
    if (s->adj_list_end[i] - g->adj_start[i] > s->actual_degree[i]) {
      RecompactifyAdjacencyList(g, s, i);
    }
    for (edge_t j = g->adj_start[i]; j < s->adj_list_end[i]; j++) {
      auto edg = s->edge_id[j];
      auto oth = g->adj[j];
      if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) + 2 <
          s->min_size) {
        if (!s->dirty_nodes_bitmap[oth] || i < oth) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
        continue;
      }
      if (s->actual_degree[i] + 1 < s->min_size) {
        if (!s->dirty_nodes_bitmap[oth] ||
            s->actual_degree[oth] + 1 >= s->min_size || i < oth) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
        continue;
      }
    }
  }
  
  FullFence();
  for (node_t i : s->dirty_nodes) {
    s->dirty_nodes_bitmap[i] = false;
  }
  s->dirty_nodes.clear();
}

//copy truss structure
void copyTruss(Graph *__restrict__ g, TrussDecompositionState *__restrict__  s_receive, TrussDecompositionState *__restrict__ s) {
    node_t node_num = g->N();
    edge_t edge_num = g->adj_start.back();

    s_receive->to_drop_end = 0;
    s_receive-> to_drop_start = 0;
    s_receive->iter_drop_start = 0;
    s_receive->edges_to_drop.clear();
    
    s_receive->edges_to_drop.resize(edge_num);
    s_receive->edge_deleted.resize((edge_num/2 + 31) >> 5);
    
    s_receive->edge_id.resize(edge_num);
    s_receive->trussness.resize(edge_num);
    s_receive->edge_triangles_count.resize(edge_num/2);
    s_receive->edge1.resize(edge_num/2);
    s_receive->edge2.resize(edge_num/2);
    
    s_receive->actual_degree.resize(node_num);
    s_receive->adj_list_fwd_start.resize(node_num);
    s_receive->adj_list_end.resize(node_num);
    s_receive->dirty_nodes_bitmap.resize(node_num);
    
    for(node_t i = 0; i < node_num; i++){
        s_receive->dirty_nodes_bitmap[i] =  s->dirty_nodes_bitmap[i];
        s_receive->actual_degree[i] = s->actual_degree[i];
        s_receive->adj_list_fwd_start[i] = s->adj_list_fwd_start[i];
        s_receive->adj_list_end[i] = s->adj_list_end[i];
    }
	
    for(edge_t i = 0; i < edge_num; i++){
      s_receive->edge_id[i]=s->edge_id[i];
      s_receive->trussness[i]=s->trussness[i];
      auto edg = s->edge_id[i];
      s_receive->edge_triangles_count[edg] =s->edge_triangles_count[edg].load(std::memory_order_relaxed);
      s_receive->edge_deleted[edg >> 5]  =s->edge_deleted[edg >> 5] ;
    }
    
    s_receive->min_size = s->min_size;
   
    for(edge_t i =0;i<edge_num/2;i++){
      s_receive->edge1[i]=s->edge1[i];
      s_receive->edge2[i]=s->edge2[i];
    }
  
}

//copy graph
void copyGraph(Graph *__restrict__ g_receive, Graph *__restrict__ g) {
	g_receive->adj.resize(g->adj.size());
	g_receive->adj_start.resize(g->adj_start.size() );
   for(node_t i = 0; i < g->adj.size() ; i++){
         g_receive->adj[i] = g->adj[i];
     }
    for(edge_t i = 0; i < g->adj_start.size() ; i++){
        g_receive->adj_start[i] =g->adj_start[i];
    }
}

void neighborList( Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               vector<edge_t> &NE, multimap<node_t, int> &degree_nodes,
                               vector<edge_t> &SOL, unordered_set<edge_t> &X_set){
  unordered_set<edge_t> SOL_set;
  unordered_set<edge_t> NE_set;
   
    for(auto &elem:SOL){
      SOL_set.insert (elem);
    } 
  
  for (auto &it: degree_nodes) {
    node_t i = it.second;
    for (edge_t j = g->adj_start[i]; j < s->adj_list_end[i]; j++) {
      //if edge not in SOL or X insert into NE
      unordered_set<node_t>::const_iterator edge_SOL = SOL_set.find (s->edge_id[j]);
      unordered_set<node_t>::const_iterator edge_X = X_set.find (s->edge_id[j]);
      unordered_set<node_t>::const_iterator edge_NE = NE_set.find (s->edge_id[j]);
      
      if ( edge_SOL == SOL_set.end() && edge_X == X_set.end() &&
        edge_NE == NE_set.end()){  
        NE.push_back(s->edge_id[j]);
        NE_set.insert(s->edge_id[j]);
      }
    }
  }

}

void findEdgeFormTriangle( Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               vector<edge_t> &triangle_edge, edge_t e_prime){

    vector <edge_t> neighbor; // store all the edges which forming a triangle with e' 
    
  node_t start = s->edge1[e_prime];
  node_t end =  s->edge2[e_prime];
 // cout<<"edge "<<e_prime<<" two nodes "<<start <<", "<<end<<endl;
  unordered_map<node_t, edge_t> map1;
  unordered_map<node_t, edge_t> map2;
  for (edge_t i = g->adj_start[start]; i < s->adj_list_end[start]; i++) {
    edge_t edg1 = s->edge_id[i];
    node_t oth = g->adj[i];
    map1.insert({oth, edg1});
  }
   for (edge_t j = g->adj_start[end]; j < s->adj_list_end[end]; j++) {
    edge_t edg1 = s->edge_id[j];
    node_t oth = g->adj[j];

    map2.insert({oth, edg1});
  }
  
  for(auto &it : map1){
      unordered_map<node_t, edge_t>::const_iterator got = map2.find (it.first);
      if ( got != map2.end() ){
          triangle_edge.push_back(it.second);
          triangle_edge.push_back(got->second);
      }
      else
      continue;
  }      

}

// Update the number of triangles for each node and recompactify the adjacency
// list if needed.
void NewUpdateTriangleCounts(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s) {

// Mark as to be removed edges whose triangle count became too low.
#pragma omp parallel for schedule(dynamic)
  for (node_t cnt = 0; cnt < s->dirty_nodes.size(); cnt++) {
    node_t i = s->dirty_nodes[cnt];
   
    if (s->adj_list_end[i] - g->adj_start[i] > s->actual_degree[i]) {
      RecompactifyAdjacencyList(g, s, i);
    }
  }
  FullFence();
  for (node_t i : s->dirty_nodes) {
    s->dirty_nodes_bitmap[i] = false;
  }
  s->dirty_nodes.clear();
  
  #pragma omp parallel for schedule(dynamic)
  for (edge_t i = s->iter_drop_start; i < s->to_drop_start; i++) {
    edge_t edg = s->edges_to_drop[i];
    if (!(s->actual_degree[s->edge1[edg]] + 1 < s->min_size &&
      s->actual_degree[s->edge2[edg]] + 1 < s->min_size)) {
      UpdateEdgeTriangles(g, s, edg);
    }
  }
  FullFence();
}


//delete the edge e
void dropEdge(Graph *__restrict__ g,
               TrussDecompositionState *__restrict__ s, edge_t e){
  auto mark_node = [&](node_t node){
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };
 
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };               
   s->edges_to_drop[s->to_drop_end.fetch_add(
                    1, std::memory_order_relaxed)] = e;
   s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
   drop_edge(e);

    s->iter_drop_start = s->to_drop_start;
    s->to_drop_start = s->to_drop_end; 
    NewUpdateTriangleCounts(g, s);
}

void graphInit(Graph *__restrict__ g,
               TrussDecompositionState *__restrict__ s){
  // Initialize data structures.
  edge_t edge_count = 0;
  edge_count = g->adj_start.back();
  s->edge_id.resize(edge_count);
  s->trussness.resize(edge_count);
  s->edge1.resize(edge_count / 2);
  s->edge2.resize(edge_count / 2);
  s->edges_to_drop.resize(edge_count);
  s->dirty_nodes_bitmap.resize(g->N());

  s->actual_degree.resize(g->N());
  s->adj_list_end.resize(g->N());
  s->adj_list_fwd_start.resize(g->N());
  
#pragma omp parallel for
  for (node_t i = 0; i < g->N(); i++) {
    s->actual_degree[i] = g->adj_start[i + 1] - g->adj_start[i];
    s->adj_list_end[i] = g->adj_start[i];
  }
  
  // We want to assign the same ID to both instances of edge {a, b} [i.e. (a, b)
  // and (b, a)], to be able to keep track of the triangle count of the edge
  // easily.

  edge_t current_id = 0;
  for (node_t i = 0; i < g->N(); i++) { 
    for (edge_t j = g->adj_start[i]; j < g->adj_start[i + 1]; j++) {
      node_t to = g->adj[j];
      if (to > i) {
        edge_t my_count = current_id++;
       
        CHECK(g->adj[s->adj_list_end[i]] == to);
        s->edge_id[s->adj_list_end[i]] = my_count;
        s->adj_list_end[i]++;

        CHECK(g->adj[s->adj_list_end[to]] == i);
        s->edge_id[s->adj_list_end[to]] = my_count;
        s->adj_list_end[to]++;

        s->edge1[my_count] = i;
        s->edge2[my_count] = to;
      }
    }
  }

  // Compute the first edge (a, b) in each adjacency list such that b > a.
#pragma omp parallel for
  for (node_t i = 0; i < g->N(); i++) {
    s->adj_list_fwd_start[i] = g->adj_start[i];
    while (s->adj_list_fwd_start[i] < s->adj_list_end[i] &&
           g->adj[s->adj_list_fwd_start[i]] < i) {
      s->adj_list_fwd_start[i]++;
    }
  }

  // Count (a, b) and (b, a) only once.
  edge_count /= 2;

  s->edge_deleted.resize((edge_count + 31) >> 5);

  // Initialize triangle counts.
  CountTrianglesFromScratch(g, s);
}

// compute the size of each truss and store into num_truss
void ComputeTrussNumberCopy(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               Graph *__restrict__ g_receive,
                               TrussDecompositionState *__restrict__ s_receive,
                                vector<size_t> &num_truss , edge_t num_runs) {
  // Initialize data structures.
  edge_t number_edge = g->adj_start.back();
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  s->to_drop_start = 0;
  s->to_drop_end = 0;

  // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
    s->edges_to_drop.resize(number_edge);

  
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  // edge_deleted is a bitvector, so it's composed of ceil(num_edges / 32)
  // uint32_t.
 
  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
  
   if(iter_num == num_runs){
    copyGraph(g_receive, g);
    copyTruss(g, s_receive, s);
   }
   
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
    if((edge_count - dropped_edges) !=0){
      num_truss.push_back(edge_count - dropped_edges);
    }
    /*
    fprintf(stderr, "Iterations for %4u-truss (%4u g->adj): %4u\n",
            s->min_size, edge_count - dropped_edges, iters);
    */
    s->min_size++;
    
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    // Ignore (now and in future iterations) nodes at the beginning of the graph
    // with degree 0. As the graph is in degeneracy order, nodes at the
    // beginning are much more likely to have degree 0 than other nodes.
    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    // Mark edges to be removed for the next truss size. If the minimum triangle
    // count of the remaining graph is larger than what would allow the
    // existence of a truss of the current size, fast-forward the truss size
    // accordingly.
    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) {
      size_t  diff = min_triangle_count + 2 -s->min_size;
         while(diff>0){
           num_truss.push_back(edge_count - dropped_edges);
           diff --;
       }           
      s->min_size = min_triangle_count + 2;
    }
  }
}


//get how many iterations it was needed 
edge_t ComputeTrussNumber(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s) {
  // Initialize data structures.
  
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  
  s->to_drop_start = 0;
  s->to_drop_end = 0;
  edge_t number_edge = g->adj_start.back();

  // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
   
    s->edges_to_drop.resize(number_edge);
  
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  // edge_deleted is a bitvector, so it's composed of ceil(num_edges / 32)
  // uint32_t.
 
  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
    //cout<<"current runs = "<<iter_num<<endl;
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }

    s->min_size++;
    
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    // Ignore (now and in future iterations) nodes at the beginning of the graph
    // with degree 0. As the graph is in degeneracy order, nodes at the
    // beginning are much more likely to have degree 0 than other nodes.
    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    // Mark edges to be removed for the next truss size. If the minimum triangle
    // count of the remaining graph is larger than what would allow the
    // existence of a truss of the current size, fast-forward the truss size
    // accordingly.
    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) { 
      s->min_size = min_triangle_count + 2;
    }
  }
  return iter_num;
}

//compute the trussness of graph G and the table T 
void ComputeTable(TrussDecompositionState *__restrict__ s_origin, 
                              Graph *__restrict__ g,TrussDecompositionState *__restrict__ s, 
                              size_t max_truss, int **tableT){
  // Initialize data structures.
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  
  s->to_drop_start = 0;
  s->to_drop_end = 0;
  edge_t number_edge = g->adj_start.back();

  // count how many edges left
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
    s->edges_to_drop.resize(number_edge);
  
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };
  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
 
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
    
    //update trussness for the graph
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           auto edg =  s->edge_id[cnt];
           s_origin->trussness[edg] =s->min_size;
           s->trussness[edg] =s->min_size;
       }
    }
    // compute the table T
    if(s->min_size>2 && s->min_size<=max_truss){
      for(size_t i =0; i< number_edge; i++){
        if(s->trussness[i] == (int)s->min_size){
          tableT[i][s->min_size-3] = s->edge_triangles_count[i].load(std::memory_order_relaxed);
        }
        else{
          tableT[i][s->min_size-3] = 0;
        }
      }
   }   
  
    s->min_size++;
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    // Ignore (now and in future iterations) nodes at the beginning of the graph
    // with degree 0. As the graph is in degeneracy order, nodes at the
    // beginning are much more likely to have degree 0 than other nodes.
    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    // Mark edges to be removed for the next truss size. If the minimum triangle
    // count of the remaining graph is larger than what would allow the
    // existence of a truss of the current size, fast-forward the truss size
    // accordingly.
    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) { 
    size_t  diff = min_triangle_count + 2 -s->min_size; // how many trussness increase
    int flag= 0;
        while(diff>0){
          //update trussness for the graph
          for (node_t i = 0; i < g->N(); i++) {
            if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
            if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
            for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
                  cnt++){
                auto edg =  s->edge_id[cnt];
                s_origin->trussness[edg] =s->min_size+flag;
                s->trussness[edg] =s->min_size+flag;
            }
          }
          //count table T
          if(s->min_size+flag>2 && s->min_size+flag<=max_truss){
              for(size_t i =0; i< number_edge; i++){
                if(s->trussness[i] == (int)(s->min_size+flag)){
                  tableT[i][s->min_size+flag-3] = s->edge_triangles_count[i].load(std::memory_order_relaxed);
                }
                else{
                  tableT[i][s->min_size+flag-3] = 0;
                }
              }
           }
          flag++;
          diff --;
       }   
      s->min_size = min_triangle_count + 2;
    }
  }
  //Update table T, tableT[i][j] is the number of triangle for edge i in trussness (j+3)
  for(size_t i =0; i< number_edge; i++){
       for(size_t j = 0; j<max_truss-3 ; j++){
          if(tableT[i][j]!=0){
           tableT[i][j] = tableT[i][j] -tableT[i][j+1];
          }
       }
  }

}

//update the trussness of edges
void UpdateTruss(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, edge_t f, 
                               int max_truss, int **tableT){
  for(int j = 0; j< max_truss-2 ; j++){
    tableT[f][j] = 0;
  }
  int old_tf = s->trussness[f];      // old_tf is the original trussness of edge f
  dropEdge(g, s, f);  //delete edge f

  s->trussness[f] = 0;
  vector<edge_t> L;
  vector<edge_t> triangle_edge;
  //for edges  that are forming a trianlge with f
  findEdgeFormTriangle(g, s, triangle_edge, f);
  // every time pass two edges that forming a triangle with edge f and update tableT
  for(int i =0;i<(int)(triangle_edge.size()-1);){
      edge_t aa = triangle_edge[i];
      edge_t bb = triangle_edge[i+1];
      int tt1 = (old_tf < s->trussness[aa]) ? min(old_tf, s->trussness[bb]): min(s->trussness[aa], s->trussness[bb]);
      i+=2;

      tableT[aa][tt1-3] -=1;
      tableT[bb][tt1-3] -=1;
      L.push_back(aa);
      L.push_back(bb);

  }

  while (!L.empty()){
      int counter = L.size()-1;
        edge_t e = L[counter];
        L.pop_back();
     
        if (tableT[e][s->trussness[e]-3] < s->trussness[e] - 2){ // trussness of edge decreases
           int t1 =s->trussness[e]; //trussness before
            while( tableT[e][s->trussness[e]-3]<s->trussness[e]  - 2){ //update edge trussness
                tableT[e][s->trussness[e]-4] = tableT[e][s->trussness[e]-4] + tableT[e][s->trussness[e]-3];
                tableT[e][s->trussness[e]-3] = 0;
                s->trussness[e] = s->trussness[e] - 1;
            }

            vector<edge_t> triangle_edgeTemp;
            //for edges  that are forming a trianlge with f
            findEdgeFormTriangle(g, s, triangle_edgeTemp, e);
            if(triangle_edgeTemp.size() ==0) continue;
            for(int i = 0; i<(int)(triangle_edgeTemp.size()-1);){
              edge_t cc = triangle_edgeTemp[i];
              edge_t dd = triangle_edgeTemp[i+1];
              int min_cc_dd = min(s->trussness[cc], s->trussness[dd]);
              int tt1 = min(t1,min_cc_dd);  //min trussness of this triangle before
              int tt2 = min(s->trussness[e],min_cc_dd ); //min trussness of this triangle after
              i+=2;
               if( tt2<tt1 ){
                  tableT[cc][tt1-3] -= 1;
                  tableT[dd][tt1-3] -= 1;
                  if(tt2 >2){
                    tableT[cc][tt2-3] +=1;
                    tableT[dd][tt2-3] +=1;
                  }
                  L.push_back(cc);
                  L.push_back(dd);
              }
            }
    }
  }
}
                       

class listMin_record{ 
  public: 
    vector<edge_t> N;
    vector<edge_t> X;
   listMin_record() : N(), X() { } 
   listMin_record(vector<edge_t> x,vector<edge_t> y) : N(x), X(y) { }
}; 


//If it reutrns true, SOL cannot be extended to a minmal k-truss 
bool Prune(Graph *__restrict__ g,
                TrussDecompositionState *__restrict__ s, int k,
                vector<edge_t> &SOL, vector<edge_t> &X, int max_truss,
               int ** tableT){
    for(auto &it : X){
      UpdateTruss(g,s,it, max_truss, tableT);
    }
    for(auto &it:SOL){
        if(s->trussness[it]<k){ return true;}
    }
    return false;
}

void ComputN(Graph *__restrict__ g,
                TrussDecompositionState *__restrict__ s,
                vector<edge_t> &SOL, vector<edge_t> &X,
                vector<edge_t> &NE){
   //if SOL isempty NE = E, sort E with the degree of node
  if(SOL.empty()){
    multimap<node_t, int> degree_nodes; //node involed in max truss
    unordered_set <node_t> node_set;
   
    for (node_t i = 0; i < g->N(); i++) {
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
        cnt++){
            auto edg = s->edge_id[cnt];
            node_t firstNode = s->edge1[edg];
            node_t secondNode = s->edge2[edg];
            unordered_set<node_t>::const_iterator gotOne = node_set.find (firstNode);
            if ( gotOne == node_set.end() ){
              degree_nodes.insert({s->actual_degree[firstNode],firstNode });

              node_set.insert(firstNode);
            }
            
            unordered_set<node_t>::const_iterator gotTwo = node_set.find (secondNode);
            if ( gotTwo == node_set.end() ){
              degree_nodes.insert({s->actual_degree[secondNode],secondNode });

              node_set.insert(secondNode);
            }
      }
    }    
    unordered_set<edge_t> NE_set;
    for (auto &it: degree_nodes){
      node_t i = it.second;
      for (edge_t j = g->adj_start[i]; j < s->adj_list_end[i]; j++) {
          unordered_set<node_t>::const_iterator edge_NE = NE_set.find (s->edge_id[j]);
          if(edge_NE == NE_set.end()){
            NE.push_back(s->edge_id[j]);
            NE_set.insert(s->edge_id[j]);
          }
      }
    }
  }
  
  else{
       unordered_set<edge_t> X_set;
      if(!X.empty()){
         for(auto &elem:X){
          X_set.insert (elem);
          s->actual_degree[s->edge1[elem]]--;
          s->actual_degree[s->edge2[elem]]--;
        } 
      }
    multimap<node_t, int> degree_nodes; //node involed in max truss
    unordered_set <node_t> node_set;
    for(auto &it: SOL){

      node_t firstNode = s->edge1[it];
      node_t secondNode = s->edge2[it];
      unordered_set<node_t>::const_iterator gotOne = node_set.find (firstNode);
      if ( gotOne == node_set.end() ){
          if(s->actual_degree[firstNode]!=0){
            degree_nodes.insert({s->actual_degree[firstNode],firstNode });
          }
        node_set.insert(firstNode);
      }
            
      unordered_set<node_t>::const_iterator gotTwo = node_set.find (secondNode);
      if ( gotTwo == node_set.end() ){
        if(s->actual_degree[secondNode]!=0){
          degree_nodes.insert({s->actual_degree[secondNode],secondNode });
        }
        node_set.insert(secondNode);
      }
    }
  
    //find all neightbours of SOL's edges
    neighborList(g, s, NE, degree_nodes,SOL, X_set);
    for(auto &it:X){
      s->actual_degree[s->edge1[it]]++;
      s->actual_degree[s->edge2[it]]++;
    }

  }
    
}

void ComputNK3(Graph *__restrict__ g,
                TrussDecompositionState *__restrict__ s,
                vector<edge_t> &SOL, vector<edge_t> &X,
                vector<edge_t> &NE){
    if(SOL.empty()){
      for (node_t i = 0; i < g->N(); i++) {
        if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
        if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
        for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
          cnt++){
          //auto edg = s->edge_id[cnt];
            NE.push_back(s->edge_id[cnt]);
        }
      }
    }
     else{
       unordered_set<edge_t> X_set;
      if(!X.empty()){
         // unordered_set <node_t> node_X;
         for(auto &elem:X){
          X_set.insert (elem);
          s->actual_degree[s->edge1[elem]]--;
          s->actual_degree[s->edge2[elem]]--;
        } 
      }
    multimap<node_t, int> degree_nodes; //node involed in max truss
    unordered_set <node_t> node_set;

    for(auto &it: SOL){
    
      node_t firstNode = s->edge1[it];
      node_t secondNode = s->edge2[it];
      unordered_set<node_t>::const_iterator gotOne = node_set.find (firstNode);
      if ( gotOne == node_set.end() ){
          if(s->actual_degree[firstNode]!=0){
            degree_nodes.insert({s->actual_degree[firstNode],firstNode });
          }
        node_set.insert(firstNode);
      }
            
      unordered_set<node_t>::const_iterator gotTwo = node_set.find (secondNode);
      if ( gotTwo == node_set.end() ){
        if(s->actual_degree[secondNode]!=0){
          degree_nodes.insert({s->actual_degree[secondNode],secondNode });
        }
        node_set.insert(secondNode);
      }
    }
   // cout<<endl;
    //find all neightbours of SOL's edges
    neighborList(g, s, NE, degree_nodes,SOL, X_set);
    for(auto &it:X){
      s->actual_degree[s->edge1[it]]++;
      s->actual_degree[s->edge2[it]]++;
    }

  }

}

void checkMinKTruss(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               int ** tableT, int k, int max_truss, vector<edge_t> &SOL,
                                vector<vector<edge_t>> &RESULT){
    
    long long  edges = s->edge_id.size();
    edges /= 2;
    for (auto &it :SOL) {
        Graph g_copy;
        TrussDecompositionState s_copy; 
        //create temp graph and tableT_temp  for edge deletion 
        copyGraph(&g_copy, g);  
        copyTruss(g, &s_copy, s);
        int **tableT_temp= AllocateDynamicArray(edges,max_truss-2);
        for(long long  i = 0;i<edges;i++){
            for(int j = max_truss-3;j>=0;j--){
                tableT_temp[i][j] = tableT[i][j];
            }
        }
        UpdateTruss(&g_copy,&s_copy, it, max_truss, tableT_temp);
        //checking the trussness of G[SOL\e]
        for(long long i =0; i< edges; i++){
            if(tableT_temp[i][k-3]>=k-2){
                FreeDynamicArray(tableT_temp,edges);
                return ;            
            }
        }
        FreeDynamicArray(tableT_temp,edges);
    }
    //it is minimal k-truss if it didn't return in last for loop
    //put the result in RESULT
    RESULT.push_back(SOL);
    return;
}

bool checkTrussnessSOL(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, int ** tableT,
                               int max_truss, vector<edge_t> &SOL, int k,
                               vector<vector<edge_t>> &RESULT){
                                    //check the trussness of SOL
    unordered_set <edge_t> SOL_set;
    for(auto &it : SOL){
        SOL_set.insert(it);
    }
    long long  edges = s->edge_id.size();
    edges /= 2;
    //create a graph with edge in SOL
    Graph g_sol;
    TrussDecompositionState s_sol;
    copyGraph(&g_sol, g);  
    copyTruss(g, &s_sol, s);

    int **tableT_sol= AllocateDynamicArray(edges,max_truss-2);
    for(long long  i = 0;i<edges;i++){
      for(int j = max_truss-3;j>=0;j--){
        tableT_sol[i][j] = tableT[i][j];
      }
    }
        //for each edge in G if they are in not in SOL remove it
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
        cnt++){
          auto edg = s->edge_id[cnt];
          unordered_set<edge_t>::const_iterator got = SOL_set.find (edg);

          if ( got == SOL_set.end()){
            UpdateTruss(&g_sol,&s_sol,edg, max_truss, tableT_sol);
          }
      }
    }
 
       int flag = 0;
       //check the if G[SOL] is a k-truss 
         for(long long i =0; i< edges; i++){
            if(tableT_sol[i][k-3]>=k-2){
                flag =1;
                break;          
            }
        }
        if(flag !=0){
            //check if G[sol] is minimal k-truss
            checkMinKTruss(&g_sol, &s_sol, tableT_sol, k,  max_truss, SOL,RESULT);
            FreeDynamicArray(tableT_sol,edges);
        
            return true;
        }
        else{
             FreeDynamicArray(tableT_sol,edges);
            return false;
        }
}

bool checkTrussnessK3(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, int ** tableT,
                               int max_truss, vector<edge_t> &SOL, int k,
                               vector<vector<edge_t>> &RESULT){

  unordered_set <node_t> node_set;
  for(auto &it:SOL){
    node_set.insert(s->edge1[it]);
    node_set.insert(s->edge2[it]);
  }
 if(node_set.size() ==3){
     RESULT.push_back(SOL);
     return true;
 }
 
  return false;
}


void updateS(stack<listMin_record> &S, listMin_record &curr, 
                    vector<edge_t> &SOL, int inter,vector<edge_t>&X_temp){
    int sizeSOL = SOL.size();
    int sizeN = curr.N.size();
    curr.X.push_back(SOL[sizeSOL-1]);
    X_temp.push_back(SOL[sizeSOL-1]);
    SOL.pop_back();
    if(sizeN >0){
      edge_t edge = curr.N.front();
      SOL.push_back(edge);
      curr.N.erase(curr.N.begin());
    }
    else{
      int sizeX = curr.X.size();
      while(sizeX>0){
          X_temp.pop_back();
          sizeX--;
      }
       curr = S.top();
       S.pop();
      while(curr.N.size() ==0){
        if(SOL.size()>0){
          SOL.pop_back();
        }
        if(!S.empty()){
          curr = S.top();
          S.pop();
        }
        inter--;
      }
      curr.X.push_back(SOL[SOL.size()-1]);
      X_temp.push_back(SOL[SOL.size()-1]);
      SOL.pop_back();
      
      edge_t edge = curr.N.front();
      SOL.push_back(edge);
      curr.N.erase(curr.N.begin());
    }

}

//List minmal k-truss
void ListMin(Graph *__restrict__ g,
                TrussDecompositionState *__restrict__ s, int k,
                int max_truss, int ** tableT, vector<vector<edge_t>> &RESULT){
  vector<edge_t> SOL;
  vector<edge_t> X_val;
  vector<edge_t> N_val;
  
  vector<edge_t> X;
  stack<listMin_record> S;
    
  int edges = s->edge_id.size()/2;
  
  ComputN(g, s, SOL, X, N_val);
  S.push(listMin_record(N_val,X));
     
  listMin_record curr;
  curr = S.top();
  S.pop();

  edge_t edge = curr.N.front();
  SOL.push_back(edge);
  curr.N.erase(curr.N.begin());
  
  int inter = 0;
  while(!S.empty() || curr.N.size()!=0){
    Graph g_copy; 
    TrussDecompositionState s_copy;
    copyGraph(&g_copy, g);  
    copyTruss(g, &s_copy, s);
    int **tableT_copy = AllocateDynamicArray(edges,max_truss-2);
    for(long long  i = 0;i<edges;i++){
      for(int j = max_truss-3;j>=0;j--){
          tableT_copy[i][j] = tableT[i][j];
      }
    }
    bool pruneStatuse = Prune(&g_copy,&s_copy,k,SOL,X_val,max_truss,tableT_copy);
    FreeDynamicArray(tableT_copy,edges);
      //Prune
    if(pruneStatuse){
      updateS(S, curr, SOL,inter,X_val);
      continue;
    }
        
    unordered_set <node_t> node_set;
    for(auto &it : SOL){
      node_set.insert(s->edge1[it]);
      node_set.insert(s->edge2[it]);
    }
    int num_nodes = node_set.size();
    
    double sol_size = (double)SOL.size();
    double check_con = (double)(num_nodes-1)*k/2;
    if(check_con<= sol_size && checkTrussnessSOL(g,s, tableT,max_truss,SOL, k,RESULT)){
      updateS(S, curr, SOL,inter,X_val);
      continue;
    }
    else{
      S.push(curr);
      N_val.clear();
      ComputN(g, s, SOL, X_val, N_val);
            
      S.push(listMin_record(N_val,X));
      curr = S.top();
      S.pop();
      edge_t edge = curr.N.front();
      SOL.push_back(edge);
      curr.N.erase(curr.N.begin());
    }
  }
}


void ListMinK3(Graph *__restrict__ g,
                TrussDecompositionState *__restrict__ s, int k,
                int max_truss, int ** tableT, vector<vector<edge_t>> &RESULT){
    vector<edge_t> SOL;
    vector<edge_t> X_val;
    vector<edge_t> N_val;
    
    vector<edge_t> X;
    stack<listMin_record> S;
    
    int edges = s->edge_id.size()/2;

    ComputNK3(g, s, SOL, X, N_val);
    S.push(listMin_record(N_val,X));
     
    listMin_record curr;
    curr = S.top();
    S.pop();
   
    edge_t edge = curr.N.front();
    SOL.push_back(edge);
    curr.N.erase(curr.N.begin());
   
    int inter = 0;
    while(!S.empty() || curr.N.size()!=0){
      Graph g_copy; 
      TrussDecompositionState s_copy;
      copyGraph(&g_copy, g);  
      copyTruss(g, &s_copy, s);
      int **tableT_copy = AllocateDynamicArray(edges,max_truss-2);
      for(long long  i = 0;i<edges;i++){
        for(int j = max_truss-3;j>=0;j--){
            tableT_copy[i][j] = tableT[i][j];
        }
      }
      bool pruneStatuse = Prune(&g_copy,&s_copy,k,SOL,X_val,max_truss,tableT_copy);
      FreeDynamicArray(tableT_copy,edges);
      //Prune
      if(pruneStatuse){
        updateS(S, curr, SOL,inter,X_val);
        continue;
      }
      if(SOL.size()==3 && checkTrussnessK3(g,s, tableT,max_truss,SOL, k,RESULT)){
        updateS(S, curr, SOL,inter,X_val);
        continue;
      }
      if(SOL.size()>=3){
        updateS(S, curr, SOL,inter,X_val);
        continue;
      }
      else{
          //extend SOL and go for each edge in SOL 
        N_val.clear();
        S.push(curr);
          
        ComputNK3(g, s, SOL, X_val, N_val);
            
        S.push(listMin_record(N_val,X));
        curr = S.top();
        S.pop();
        edge_t edge = curr.N.front();
        SOL.push_back(edge);
        curr.N.erase(curr.N.begin());
      }
    }
  }


void Run(int argc, char **argv) {
  Graph g;
  g.adj_start.init(FileFromStorageDir(FLAGS_storage_dir, "adj_start.bin"));
  g.adj.init(FileFromStorageDir(FLAGS_storage_dir, "edges.bin"));
  
  std::chrono::high_resolution_clock::time_point reading;
  g.Read(FLAGS_filename, ParseGraphFormat(FLAGS_format),
         GraphPermutation::kDegeneracyOrder,
         /*forward_only=*/false, &reading);
          
  TrussDecompositionState s;
  s.SetStorageDir(FLAGS_storage_dir);
 
  int k = atoi(argv[1]);
  graphInit(&g, &s); // INIT graph 

  long long  edges = s.edge_id.size();
  edges /= 2;

  
  Graph g_copy, g_copy2;  // save original graph for counting max_truss
  TrussDecompositionState s_copy, s_copy2;
  
  copyGraph(&g_copy, &g);  
  copyTruss(&g, &s_copy, &s);

  ComputeTrussNumber(&g_copy, &s_copy); // to get the max truss of g
  
  int max_truss =  s_copy.min_size-2;  //max truss to set the size of 2d table T
 
  cout<<"number of edges = "<<edges<<" , max_truss = "<<max_truss<<endl;
  
  //table T store #. triangle in current truss for each edge
  int **tableT = AllocateDynamicArray(edges,max_truss-2);
  Graph g_max;  // max truss stock
  TrussDecompositionState s_max;
   // copy g and s 
  copyGraph(&g_copy2, &g);  
  copyTruss(&g, &s_copy2, &s);
  //compute the trussness of graph G and the table T 
  ComputeTable(&s,&g_copy2, &s_copy2, max_truss, tableT); 
  
   //remvoe all edges trussness <k 
    copyGraph(&g_max, &g);  
    copyTruss(&g, &s_max, &s);
    
    for (node_t i = 0; i < g.N(); i++) {
      if (s.adj_list_end[i] == s.adj_list_fwd_start[i]) continue;
      if(s.adj_list_end[i] <=s.adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s.adj_list_fwd_start[i]; cnt < s.adj_list_end[i];
         cnt++){
          auto edg = s.edge_id[cnt];
          if(s.trussness[edg] < k){
            UpdateTruss(&g_max,&s_max,edg, max_truss, tableT);
          }
       }
    }
    vector<vector<edge_t>> RESULT;
   //input the k-truss 
   auto minKtruss = std::chrono::high_resolution_clock::now();
 
  if(k>3){
    ListMin(&g_max,&s_max, k, max_truss, tableT, RESULT);
  }
  else{
      ListMinK3(&g_max,&s_max, k, max_truss, tableT, RESULT);
  }
   auto end = std::chrono::high_resolution_clock::now();
   FreeDynamicArray(tableT,edges);

    cout<<"Number of min "<<k<<" truss  = "<<RESULT.size()<<endl;

    ofstream output_file3;
	output_file3.open ("minK"+to_string(k)+"truss.dat");
     for(auto &it:RESULT){
      for(auto &it2: it){
              output_file3<<it2<<"    ";
          }
          output_file3<<endl;
    }
	output_file3.close();
    
 fprintf(stderr, "Time for find min Ktruss: %ld ms\n",
          std::chrono::duration_cast<std::chrono::milliseconds>(end- minKtruss )
              .count());
              
  std::string line = "shd31/./shd 0 ";
    line += "minK"+to_string(k)+"truss.dat";
    system(line.c_str());  
 
}

int main(int argc, char **argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  Run(argc, argv);
}
