#include <gflags/gflags.h>

#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

#include <map>
#include <string>
#include <iterator>
#include <vector> 
#include <set>
#include <unordered_set>
#include <unordered_map>

#include <chrono>  // NOLINT
#include <igraph.h>
#include <algorithm>
#include "common.h"           // NOLINT
#include "graph.h"            // NOLINT
#include "intersect_edges.h"  // NOLINT
#include "mmapped_vector.h"   // NOLINT

#include <boost/functional/hash.hpp>

using namespace std;


DEFINE_string(filename, "", "Input filename. Omit for stdin.");
DEFINE_string(format, "nde",
              "Input file format. Valid values are nde, bin and sbin.");
DEFINE_string(storage_dir, "",
              "Directory for storage of working variables. Leave empty to use "
              "main memory.");

using edge_t = uint32_t;
using node_t = uint32_t;

typedef pair<edge_t, edge_t> Pair;
using Graph = GraphT<node_t, edge_t>;

int **AllocateDynamicArray( long long nRows, int nCols){
      int **dynamicArray = new(nothrow) int*[nRows];
      for( long long i = 0 ; i < nRows ; i++ ){
        dynamicArray[i] = new(nothrow)  int [nCols];
      }
      return dynamicArray;
}

void FreeDynamicArray(int** dArray, long long  nRows){
	for( long long i = 0 ; i < nRows ; i++ ){
		delete [] dArray[i] ; 
    }        
      delete [] dArray;
}

bool cmp(pair<int, int>& a, 
         pair<int, int>& b) { 
    return a.second > b.second; 
}

bool cmpDouble(pair<int, double>& a, 
         pair<int, double>& b) { 
    return a.second > b.second; 
}

struct TrussDecompositionState {
  // Graph - node-sized vectors
  mmapped_vector<node_t> actual_degree;
  mmapped_vector<edge_t> adj_list_end;
  mmapped_vector<edge_t> adj_list_fwd_start;

  // Algorithm support structures (node-sized)
  mmapped_vector<node_t> dirty_nodes;
  mmapped_vector<uint8_t> dirty_nodes_bitmap;

  // Graph - edge-sized vectors
  mmapped_vector<edge_t> edge_id;
  mmapped_vector<int> trussness;

  // Algorithm support structures (edge-sized)
  mmapped_vector<node_t> edge1;
  mmapped_vector<node_t> edge2;
  mmapped_vector<uint32_t> edge_deleted;
  mmapped_vector<std::atomic<node_t>> edge_triangles_count;

  mmapped_vector<edge_t> edges_to_drop;
  edge_t to_drop_start = 0;
  std::atomic<edge_t> to_drop_end{0};
  edge_t iter_drop_start = 0;
  node_t min_size = 2;

  void SetStorageDir(const std::string &storage_dir) {
    actual_degree.init(FileFromStorageDir(storage_dir, "degrees.bin"));
    adj_list_end.init(FileFromStorageDir(storage_dir, "adj_list_end.bin"));
    adj_list_fwd_start.init(
        FileFromStorageDir(storage_dir, "adj_list_fwd_start.bin"));
    dirty_nodes.init(FileFromStorageDir(storage_dir, "dirty_nodes.bin"));
    dirty_nodes_bitmap.init(
        FileFromStorageDir(storage_dir, "dirty_nodes_bitmap.bin"));
    edge_id.init(FileFromStorageDir(storage_dir, "edge_id.bin"));
    trussness.init(FileFromStorageDir(storage_dir, "trussness.bin"));
    edge1.init(FileFromStorageDir(storage_dir, "edge1.bin"));
    edge2.init(FileFromStorageDir(storage_dir, "edge2.bin"));
    edge_deleted.init(FileFromStorageDir(storage_dir, "edge_deleted.bin"));
    edge_triangles_count.init(
        FileFromStorageDir(storage_dir, "edge_triangles_count.bin"));
    edges_to_drop.init(FileFromStorageDir(storage_dir, "edges_to_drop.bin"));
  }
};

#if defined(__AVX2__)
static __m256i avx2_zero = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 0, 0);
static __m256i avx2_one = _mm256_set_epi32(1, 1, 1, 1, 1, 1, 1, 1);
static __m256i bv_offsets_mask =
  _mm256_set_epi32(0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f);

__m256i ComputeCompress256Mask(unsigned int mask) {
  const uint32_t identity_indices = 0x76543210;
  uint32_t wanted_indices = _pext_u32(identity_indices, mask);
  __m256i fourbitvec = _mm256_set1_epi32(wanted_indices);
  __m256i shufmask = _mm256_srlv_epi32(
    fourbitvec, _mm256_set_epi32(28, 24, 20, 16, 12, 8, 4, 0));
  shufmask = _mm256_and_si256(shufmask, _mm256_set1_epi32(0x0F));
  return shufmask;
}
#endif

void printGraph(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s){
    size_t min_triangle = s->min_size;
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
          const node_t j = g->adj[cnt];
          auto edg = s->edge_id[cnt];
          size_t count =s->edge_triangles_count[edg].load(std::memory_order_relaxed);
          if(count < min_triangle) min_triangle = count;
          std::cout<<"edge id = "<<edg <<" : "<<i<<"->"<<j<<" support = "<<count<<", trussness = "<<s->trussness[edg]<<std::endl;
       }
    }
}

// Re-compute the triangle count for an edge that has had adjacent edges
// removed.
void UpdateEdgeTriangles(Graph *__restrict__ g,
                         TrussDecompositionState *__restrict__ s, edge_t edg) {
  if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) == 0) return;
  node_t i = s->edge1[edg];
  node_t j = s->edge2[edg];
  edge_t inters = 0;
  IntersectEdges(
      g, g->adj_start[i], s->adj_list_end[i], g->adj_start[j],
      s->adj_list_end[j], [&](edge_t k, edge_t l) {
        edge_t edg2 = s->edge_id[k];
        edge_t edg3 = s->edge_id[l];
        bool edge2_deleted = s->edge_deleted[edg2 >> 5] & (1U << (edg2 & 0x1F));
        bool edge3_deleted = s->edge_deleted[edg3 >> 5] & (1U << (edg3 & 0x1F));

        if ((!edge2_deleted || edg2 > edg) && (!edge3_deleted || edg3 > edg)) {
          inters++;
          if (!edge2_deleted) {
            s->edge_triangles_count[edg2].fetch_sub(1,
                                                    std::memory_order_relaxed);
          }
          if (!edge3_deleted) {
            s->edge_triangles_count[edg3].fetch_sub(1,
                                                    std::memory_order_relaxed);
          }
        }
        return inters <
               s->edge_triangles_count[edg].load(std::memory_order_relaxed);
      });
}

void CountTrianglesFromScratch(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s) {                             

  s->edge_triangles_count.resize(g->adj_start.back() / 2);

  s->min_size =2;
#pragma omp parallel for schedule(guided)
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    thread_local std::vector<node_t> local_counts;
    local_counts.clear();
    local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      const node_t j = g->adj[cnt];
      IntersectEdges(g, cnt + 1, s->adj_list_end[i], s->adj_list_fwd_start[j],
                     s->adj_list_end[j], [&](edge_t k, edge_t l) {
                       edge_t edg3 = s->edge_id[l];
                       local_counts[cnt - s->adj_list_fwd_start[i]]++;
                       local_counts[k - s->adj_list_fwd_start[i]]++;
                       s->edge_triangles_count[edg3].fetch_add(
                           1, std::memory_order_relaxed);
                       return true;
                     });
    }
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      s->edge_triangles_count[s->edge_id[cnt]].fetch_add(
          local_counts[cnt - s->adj_list_fwd_start[i]],
          std::memory_order_relaxed);
    }
  }
  FullFence();
#pragma omp parallel for schedule(guided)
  for (node_t i = 0; i < g->N(); i++) {
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++) {
      auto edg = s->edge_id[cnt];
      if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) + 2 <
          s->min_size) {
        s->edges_to_drop[s->to_drop_end.fetch_add(
            1, std::memory_order_relaxed)] = edg;
      }
    }
  }
  FullFence();
}

// Remove gaps in the adjacency lists after removing edges.
void RecompactifyAdjacencyList(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s,
                               node_t i) {
  if (s->actual_degree[i] == 0) {
    s->adj_list_end[i] = s->adj_list_fwd_start[i] = g->adj_start[i];
    return;
  }
  s->adj_list_fwd_start[i] = g->adj_start[i];
  
  edge_t j = g->adj_start[i];
 
  node_t pos = g->adj_start[i];
#if defined(__AVX2__)
  __m256i avx2_i = _mm256_castps_si256(_mm256_broadcast_ss((float *)&i));
  for (; j + 7 < s->adj_list_end[i]; j += 8) {
    __m256i edge_ids = _mm256_loadu_si256((__m256i *)&s->edge_id[j]);
   
    __m256i vertices = _mm256_loadu_si256((__m256i *)&g->adj[j]);

    __m256i bv_offsets = _mm256_srai_epi32(edge_ids, 5);
    __m256i bv_shifts = _mm256_and_si256(edge_ids, bv_offsets_mask);

    __m256i deleted = _mm256_i32gather_epi32(
        (int *)s->edge_deleted.data(), bv_offsets, sizeof(s->edge_deleted[0]));
    deleted = _mm256_srav_epi32(deleted, bv_shifts);
    deleted = _mm256_and_si256(deleted, avx2_one);
    __m256i present_mask = _mm256_cmpeq_epi32(deleted, avx2_zero);

    __m256i greater_mask = _mm256_cmpgt_epi32(avx2_i, vertices);
    greater_mask = _mm256_and_si256(greater_mask, present_mask);

    uint32_t mask = _mm256_movemask_ps(_mm256_castsi256_ps(present_mask));

    if (!mask) continue;
    __m256i compress256_mask =
        ComputeCompress256Mask(_mm256_movemask_epi8(present_mask));
    edge_ids = _mm256_permutevar8x32_epi32(edge_ids, compress256_mask);
    vertices = _mm256_permutevar8x32_epi32(vertices, compress256_mask);

    _mm256_storeu_si256((__m256i *)&g->adj[pos], vertices);
    _mm256_storeu_si256((__m256i *)&s->edge_id[pos], edge_ids);
    pos += __builtin_popcount(mask);

    uint32_t greater_count = __builtin_popcount(
        _mm256_movemask_ps(_mm256_castsi256_ps(greater_mask)));
    s->adj_list_fwd_start[i] += greater_count;
  }
#endif

  for (; j < s->adj_list_end[i]; j++) {
    auto edg = s->edge_id[j];
    if (s->edge_deleted[edg >> 5] & (1U << (edg & 0x1F))) continue;
    if (g->adj[j] < i) s->adj_list_fwd_start[i]++;
    g->adj[pos] = g->adj[j];
    s->edge_id[pos] = s->edge_id[j];
    pos++;
  }
  s->adj_list_end[i] = pos;
}


// Update the number of triangles for each node and recompactify the adjacency
// list if needed.
void UpdateTriangleCounts(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s) {

#pragma omp parallel for schedule(dynamic)
  for (edge_t i = s->iter_drop_start; i < s->to_drop_start; i++) {
    edge_t edg = s->edges_to_drop[i];
    if (!(s->actual_degree[s->edge1[edg]] + 1 < s->min_size &&
          s->actual_degree[s->edge2[edg]] + 1 < s->min_size)) {
      UpdateEdgeTriangles(g, s, edg);
    }
  }
  FullFence();

  // Mark as to be removed edges whose triangle count became too low.
#pragma omp parallel for schedule(dynamic)
  for (node_t cnt = 0; cnt < s->dirty_nodes.size(); cnt++) {
    node_t i = s->dirty_nodes[cnt];
    if (s->adj_list_end[i] - g->adj_start[i] > s->actual_degree[i]) {
      RecompactifyAdjacencyList(g, s, i);
    }
    for (edge_t j = g->adj_start[i]; j < s->adj_list_end[i]; j++) {
      auto edg = s->edge_id[j];
      auto oth = g->adj[j];
      if (s->edge_triangles_count[edg].load(std::memory_order_relaxed) + 2 <
          s->min_size) {
        if (!s->dirty_nodes_bitmap[oth] || i < oth) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
        continue;
      }
      if (s->actual_degree[i] + 1 < s->min_size) {
        if (!s->dirty_nodes_bitmap[oth] ||
            s->actual_degree[oth] + 1 >= s->min_size || i < oth) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
        continue;
      }
    }
  }
  
  FullFence();
  for (node_t i : s->dirty_nodes) {
    s->dirty_nodes_bitmap[i] = false;
  }
  s->dirty_nodes.clear();
}

//copy truss structure
void copyTruss(Graph *__restrict__ g, TrussDecompositionState *__restrict__  s_receive, TrussDecompositionState *__restrict__ s) {
    node_t node_num = g->N();
    edge_t edge_num = g->adj_start.back();

    s_receive->to_drop_end = 0;
    s_receive-> to_drop_start = 0;
    s_receive->iter_drop_start = 0;
    s_receive->edges_to_drop.clear();
    
    s_receive->edges_to_drop.resize(edge_num);
    s_receive->edge_deleted.resize((edge_num/2 + 31) >> 5);
    
    s_receive->edge_id.resize(edge_num);
    s_receive->trussness.resize(edge_num);
    s_receive->edge_triangles_count.resize(edge_num/2);
    s_receive->edge1.resize(edge_num/2);
    s_receive->edge2.resize(edge_num/2);
    
    s_receive->actual_degree.resize(node_num);
    s_receive->adj_list_fwd_start.resize(node_num);
    s_receive->adj_list_end.resize(node_num);
    s_receive->dirty_nodes_bitmap.resize(node_num);
    
    for(node_t i = 0; i < node_num; i++){
        s_receive->dirty_nodes_bitmap[i] =  s->dirty_nodes_bitmap[i];
        s_receive->actual_degree[i] = s->actual_degree[i];
        s_receive->adj_list_fwd_start[i] = s->adj_list_fwd_start[i];
        s_receive->adj_list_end[i] = s->adj_list_end[i];
    }
    for(edge_t i = 0; i < edge_num; i++){
      s_receive->edge_id[i]=s->edge_id[i];
      s_receive->trussness[i]=s->trussness[i];
      auto edg = s->edge_id[i];
      s_receive->edge_triangles_count[edg] =s->edge_triangles_count[edg].load(std::memory_order_relaxed);
      s_receive->edge_deleted[edg >> 5]  =s->edge_deleted[edg >> 5] ;
    }
    
    s_receive->min_size = s->min_size;
   
    for(edge_t i =0;i<edge_num/2;i++){
      s_receive->edge1[i]=s->edge1[i];
      s_receive->edge2[i]=s->edge2[i];
    }
  
}

//copy graph
void copyGraph(Graph *__restrict__ g_receive, Graph *__restrict__ g) {
	g_receive->adj.resize(g->adj.size());
	g_receive->adj_start.resize(g->adj_start.size() );
   for(node_t i = 0; i < g->adj.size() ; i++){
         g_receive->adj[i] = g->adj[i];
     }
    for(edge_t i = 0; i < g->adj_start.size() ; i++){
        g_receive->adj_start[i] =g->adj_start[i];
    }
}

//find edges forming triangle with edge e_prime
void findEdgeFormTriangle( Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               vector<edge_t> &triangle_edge, edge_t e_prime){
    // store all the edges which forming a triangle with e' 
    vector <edge_t> neighbor; 
    
  // find all edges which forming a triangle with e'  
  node_t start = s->edge1[e_prime];
  node_t end =  s->edge2[e_prime];
  unordered_map<node_t, edge_t> map1;
  unordered_map<node_t, edge_t> map2;
  for (edge_t i = g->adj_start[start]; i < s->adj_list_end[start]; i++) {
    edge_t edg1 = s->edge_id[i];
    node_t oth = g->adj[i];
    map1.insert({oth, edg1});
  }
   for (edge_t j = g->adj_start[end]; j < s->adj_list_end[end]; j++) {
    edge_t edg1 = s->edge_id[j];
    node_t oth = g->adj[j];
    map2.insert({oth, edg1});
  }
  
  for(auto &it : map1){
      unordered_map<node_t, edge_t>::const_iterator got = map2.find (it.first);
      if ( got != map2.end() ){
          triangle_edge.push_back(it.second);
          triangle_edge.push_back(got->second);
      }
      else
        continue;
  }      
 
}

// Update the number of triangles for each node and recompactify the adjacency
// list if needed.
void NewUpdateTriangleCounts(Graph *__restrict__ g,
                          TrussDecompositionState *__restrict__ s) {

// Mark as to be removed edges whose triangle count became too low.
#pragma omp parallel for schedule(dynamic)
  for (node_t cnt = 0; cnt < s->dirty_nodes.size(); cnt++) {
    node_t i = s->dirty_nodes[cnt];
   
    if (s->adj_list_end[i] - g->adj_start[i] > s->actual_degree[i]) {
      RecompactifyAdjacencyList(g, s, i);
    }
  }
  FullFence();
  for (node_t i : s->dirty_nodes) {
    s->dirty_nodes_bitmap[i] = false;
  }
  s->dirty_nodes.clear();
  
  #pragma omp parallel for schedule(dynamic)
  for (edge_t i = s->iter_drop_start; i < s->to_drop_start; i++) {
    edge_t edg = s->edges_to_drop[i];
  
    if (!(s->actual_degree[s->edge1[edg]] + 1 < s->min_size &&
          s->actual_degree[s->edge2[edg]] + 1 < s->min_size)) {
      UpdateEdgeTriangles(g, s, edg);
    }
  }
  FullFence();
}


//delete the edge e
void dropEdge(Graph *__restrict__ g,
               TrussDecompositionState *__restrict__ s, edge_t e){
  auto mark_node = [&](node_t node){
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };
 
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };         
   s->edges_to_drop[s->to_drop_end.fetch_add(
                    1, std::memory_order_relaxed)] = e;
   s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
   drop_edge(e);

    s->iter_drop_start = s->to_drop_start;
    s->to_drop_start = s->to_drop_end; 
    NewUpdateTriangleCounts(g, s);
}


void graphInit(Graph *__restrict__ g,
               TrussDecompositionState *__restrict__ s){
  // Initialize data structures.
  edge_t edge_count = 0;
  edge_count = g->adj_start.back();
  s->edge_id.resize(edge_count);
  s->trussness.resize(edge_count);
  s->edge1.resize(edge_count / 2);
  s->edge2.resize(edge_count / 2);
  s->edges_to_drop.resize(edge_count);
  s->dirty_nodes_bitmap.resize(g->N());

  s->actual_degree.resize(g->N());
  s->adj_list_end.resize(g->N());
  s->adj_list_fwd_start.resize(g->N());
 
#pragma omp parallel for
  for (node_t i = 0; i < g->N(); i++) {
    s->actual_degree[i] = g->adj_start[i + 1] - g->adj_start[i];
    s->adj_list_end[i] = g->adj_start[i];
  }
  
  // We want to assign the same ID to both instances of edge {a, b} [i.e. (a, b)
  // and (b, a)], to be able to keep track of the triangle count of the edge
  // easily.
  edge_t current_id = 0;
  for (node_t i = 0; i < g->N(); i++) { 
    for (edge_t j = g->adj_start[i]; j < g->adj_start[i + 1]; j++) {
      node_t to = g->adj[j];
      if (to > i) {
        edge_t my_count = current_id++;
       
        CHECK(g->adj[s->adj_list_end[i]] == to);
        s->edge_id[s->adj_list_end[i]] = my_count;
        s->adj_list_end[i]++;

        CHECK(g->adj[s->adj_list_end[to]] == i);
        s->edge_id[s->adj_list_end[to]] = my_count;
        s->adj_list_end[to]++;

        s->edge1[my_count] = i;
        s->edge2[my_count] = to;
      }
    }
  }

  //Compute the first edge (a, b) in each adjacency list such that b > a.
  #pragma omp parallel for
  for (node_t i = 0; i < g->N(); i++) {
    s->adj_list_fwd_start[i] = g->adj_start[i];
    while (s->adj_list_fwd_start[i] < s->adj_list_end[i] &&
           g->adj[s->adj_list_fwd_start[i]] < i) {
      s->adj_list_fwd_start[i]++;
    }
  }
  // Count (a, b) and (b, a) only once.
  edge_count /= 2;

  // edge_deleted is a bitvector, so it's composed of ceil(num_edges / 32)
  // uint32_t.
  s->edge_deleted.resize((edge_count + 31) >> 5);

  // Initialize triangle counts.
  CountTrianglesFromScratch(g, s);
}


// compute the size of each truss and store into num_truss
void ComputeTrussNumberBase(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               Graph *__restrict__ g_receive,
                               TrussDecompositionState *__restrict__ s_receive,
                                vector<size_t> &num_truss , edge_t num_runs) {
  // Initialize data structures.
  edge_t number_edge = g->adj_start.back();
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  s->to_drop_start = 0;
  s->to_drop_end = 0;

  // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
    s->edges_to_drop.resize(number_edge);

  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
  
   if(iter_num == num_runs){
    copyGraph(g_receive, g);
    copyTruss(g, s_receive, s);
   }
   
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
    if((edge_count - dropped_edges) !=0){
      num_truss.push_back(edge_count - dropped_edges);
    }

    s->min_size++;
    
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    // Ignore (now and in future iterations) nodes at the beginning of the graph
    // with degree 0. As the graph is in degeneracy order, nodes at the
    // beginning are much more likely to have degree 0 than other nodes.
    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    // Mark edges to be removed for the next truss size. If the minimum triangle
    // count of the remaining graph is larger than what would allow the
    // existence of a truss of the current size, fast-forward the truss size
    // accordingly.
    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) {
      size_t  diff = min_triangle_count + 2 -s->min_size;
         while(diff>0){
           num_truss.push_back(edge_count - dropped_edges);
           diff --;
       }           
      s->min_size = min_triangle_count + 2;
    }
  }
}


//get how many iterations it was needed 
edge_t ComputeTrussNumber(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s) {
  // Initialize data structures.
  
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  
  s->to_drop_start = 0;
  s->to_drop_end = 0;
  edge_t number_edge = g->adj_start.back();

  // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
   
    s->edges_to_drop.resize(number_edge);
  
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
    s->min_size++;
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    edge_t min_triangle_count = g->N();
    
  #pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) { 
      s->min_size = min_triangle_count + 2;
    }
  }
  return iter_num;
}

//compute clustering coefficent
igraph_real_t clusteringCoeff(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s){
   size_t edge_count = 0;
     // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    } 

   igraph_t igraph_g;
   igraph_real_t res1;

   igraph_vector_t v;
   igraph_vector_init(&v, edge_count*2);
   size_t n=0;
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
    for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
      cnt++){
        const node_t j = g->adj[cnt];
        VECTOR(v)[n] = i;
        n++;
        VECTOR(v)[n] = j;
        n++;
       }
    }
  igraph_create(&igraph_g, &v, 0, 0);
  igraph_transitivity_undirected(&igraph_g, &res1, IGRAPH_TRANSITIVITY_NAN);

  igraph_vector_destroy(&v);
  igraph_destroy(&igraph_g);
  return res1;
}

//compute the trussness of graph G and the table T 
void ComputeTable(TrussDecompositionState *__restrict__ s_origin, 
                              Graph *__restrict__ g,TrussDecompositionState *__restrict__ s, 
                              size_t max_truss, int **tableT, Graph *__restrict__ g_receive,
                              TrussDecompositionState *__restrict__ s_receive, vector<size_t> &num_truss, edge_t runs){
  // Initialize data structures.
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  
  s->to_drop_start = 0;
  s->to_drop_end = 0;
  edge_t number_edge = g->adj_start.back();

  // count how many edges left
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
    s->edges_to_drop.resize(number_edge);
  
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
     if(iter_num == runs){
      copyGraph(g_receive, g);
      copyTruss(g, s_receive, s);
   }
  
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
     if((edge_count - dropped_edges) !=0){
      num_truss.push_back(edge_count - dropped_edges);
    }

    //update trussness for the graph
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           auto edg =  s->edge_id[cnt];
           s_origin->trussness[edg] =s->min_size;
           s->trussness[edg] =s->min_size;
       }
    }
    // compute the table T
    if(s->min_size>2 && s->min_size<=max_truss){
      for(size_t i =0; i< number_edge; i++){
        if(s->trussness[i] == (int)s->min_size){
          tableT[i][s->min_size-3] = s->edge_triangles_count[i].load(std::memory_order_relaxed);
        }
        else{
          tableT[i][s->min_size-3] = 0;
        }
      }
   }   
  
    s->min_size++;
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) { 
    size_t  diff = min_triangle_count + 2 -s->min_size; // how many trussness increase
    int flag= 0;
        while(diff>0){
          num_truss.push_back(edge_count - dropped_edges);
          //update trussness for the graph
          for (node_t i = 0; i < g->N(); i++) {
            if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
            if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
            for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
                  cnt++){
                auto edg =  s->edge_id[cnt];
                s_origin->trussness[edg] =s->min_size+flag;
                s->trussness[edg] =s->min_size+flag;
            }
          }
          //count table T
          if(s->min_size+flag>2 && s->min_size+flag<=max_truss){
              for(size_t i =0; i< number_edge; i++){
                if(s->trussness[i] == (int)(s->min_size+flag)){
                  tableT[i][s->min_size+flag-3] = s->edge_triangles_count[i].load(std::memory_order_relaxed);
                }
                else{
                  tableT[i][s->min_size+flag-3] = 0;
                }
              }
           }
          flag++;
          diff --;
       }   
      s->min_size = min_triangle_count + 2;
    }
  }
  //Update table T, tableT[i][j] is the number of triangle for edge i in trussness (j+3)
  for(size_t i =0; i< number_edge; i++){
       for(size_t j = 0; j<max_truss-3 ; j++){
          if(tableT[i][j]!=0){
           tableT[i][j] = tableT[i][j] -tableT[i][j+1];
          }
       }
  }

}

//update the trussness of edges
void UpdateTruss(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, edge_t f, 
                               int max_truss, int **tableT){
  for(int j = 0; j< max_truss-2 ; j++){
    tableT[f][j] = 0;
  }
  int old_tf = s->trussness[f];      // old_tf is the original trussness of edge f
  dropEdge(g, s, f);  //delete edge f

  s->trussness[f] = 0;
  vector<edge_t> L;
  vector<edge_t> triangle_edge;
  //for edges  that are forming a trianlge with f
   
  findEdgeFormTriangle(g, s, triangle_edge, f);
  //every time pass two edges that forming a triangle with edge f and update tableT
  for(int i =0;i<(int)(triangle_edge.size()-1);){
      edge_t aa = triangle_edge[i];
      edge_t bb = triangle_edge[i+1];
      int tt1 = (old_tf < s->trussness[aa]) ? min(old_tf, s->trussness[bb]): min(s->trussness[aa], s->trussness[bb]);
      int tt2 = 0;
      i+=2;
    if( tt2<tt1 ){
      tableT[aa][tt1-3] -=1;
      tableT[bb][tt1-3] -=1;
      L.push_back(aa);
      L.push_back(bb);
    }
  }

  while (!L.empty()){
      int counter = L.size()-1;
        edge_t e = L[counter];
        L.pop_back();
  
        if (tableT[e][s->trussness[e]-3] < s->trussness[e] - 2){ // trussness of edge decreases
           int t1 =s->trussness[e]; //trussness before
            while( tableT[e][s->trussness[e]-3]<s->trussness[e]  - 2){ //update edge trussness
              if(s->trussness[e]-4 >=0){
                tableT[e][s->trussness[e]-4] = tableT[e][s->trussness[e]-4] + tableT[e][s->trussness[e]-3];
              }
                tableT[e][s->trussness[e]-3] = 0;
                s->trussness[e] = s->trussness[e] - 1;
            }
             s->edge_triangles_count[e] = tableT[e][s->trussness[e]-3];
            triangle_edge.clear();
            //for edges  that are forming a trianlge with f
            findEdgeFormTriangle(g, s, triangle_edge, e);
            if(triangle_edge.size() ==0) continue;
            for(int i = 0; i<(int)(triangle_edge.size()-1);){
              edge_t cc = triangle_edge[i];
              edge_t dd = triangle_edge[i+1];
              int min_cc_dd = min(s->trussness[cc], s->trussness[dd]);
              int tt1 = min(t1,min_cc_dd);  //min trussness of this triangle before
              int tt2 = min(s->trussness[e],min_cc_dd ); //min trussness of this triangle after
              i+=2;
               if( tt2<tt1 ){
                  tableT[cc][tt1-3] -= 1;
                  tableT[dd][tt1-3] -= 1;
                  if(tt2 >2){
                    tableT[cc][tt2-3] +=1;
                    tableT[dd][tt2-3] +=1;
                  }
                  L.push_back(cc);
                  L.push_back(dd);
              }
            }
    }
  }
}
                       

// select an edge e' wihch triangle count = k-2 and delete an edges  forming a triangle with e' 
edge_t findEdgeDelete(Graph *__restrict__  g,
                               TrussDecompositionState *__restrict__ s,
                               vector<int> &KTruss, int max_truss, int ** tableT) {

  for (int i = 0; i< (int)(KTruss.size());i++){
    edge_t e_prime= KTruss[i];  //edge e'
    edge_t e = e_prime;

    int count =  tableT[e_prime][max_truss-3];
    if(count==0) continue;    
    if (count + 2 == max_truss) { 
      vector <edge_t> all_neighbor; // store all the edges which forming a triangle with e' 
      findEdgeFormTriangle(g, s, all_neighbor, e_prime);
      int min_triangle = max_truss - 2;
      //find edge e where it forming a triangle with e_prime and it trussness =  max_truss
      for(auto & element : all_neighbor){
          if(tableT[element][max_truss-3] >=max_truss-2 ){
               if(tableT[element][max_truss-3] >min_triangle){
                  min_triangle = tableT[element][max_truss-3];
                  e  =  element;
                }
          }
      }
      return e;
    }
  }
  cout<<"Can't find any edges to delete"<<endl;
  return -1;
}

//use in SNH algorithm 
void findNeighborsCurrentTrussSET( Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               unordered_set<Pair, boost::hash<Pair>> &triangle_edge, 
                               edge_t e_prime,  int ** tableT, int k, vector<edge_t> &edgeList){

  node_t start = s->edge1[e_prime];
  node_t end =  s->edge2[e_prime];
  unordered_map<node_t, edge_t> map1;
  unordered_map<node_t, edge_t> map2;
  for (edge_t i = g->adj_start[start]; i < s->adj_list_end[start]; i++) {
    edge_t edg1 = s->edge_id[i];
    node_t oth = g->adj[i];
    if(s->trussness[edg1]>=k){
      map1.insert({oth, edg1});
    }
  }
  for (edge_t j = g->adj_start[end]; j < s->adj_list_end[end]; j++) {
    edge_t edg1 = s->edge_id[j];
    node_t oth = g->adj[j];
    if(s->trussness[edg1]>=k){
      map2.insert({oth, edg1});
    }
  }
  for(auto &it : map1){
      unordered_map<node_t, edge_t>::const_iterator got_tri = map2.find (it.first);
      if ( got_tri != map2.end() ){
           //Find edges forming triangle with e_prime in current_truss
          if(s->trussness[it.second]  >= k && s->trussness[got_tri->second]  >= k){
            //If the edge not in vector insert
            pair <edge_t,edge_t> insert_pair;
            if(it.second < got_tri->second){
              insert_pair = make_pair (it.second,got_tri->second);
            }
            else{
              insert_pair = make_pair (got_tri->second,it.second);
            }
            unordered_set<Pair, boost::hash<Pair>>::const_iterator check_exit = triangle_edge.find (insert_pair);

            if ( check_exit == triangle_edge.end() ){
              triangle_edge.insert(insert_pair);
              edgeList.push_back(it.second);
              edgeList.push_back(got_tri->second);
            }
          }
      }
      else
      continue;
  }   
}

void countNumberTrussness(int current_truss,vector<size_t> &num_trussNew ,
                                            long long num_edge, int ** tableT){

 int *temp_table = new int[num_edge];
  for( long long i =0; i< num_edge; i++){
       temp_table[i] = 0;
  }

   long long size_maxTruss =0;
  // go through the last col and check how many edges are in this truss
  for( long long i =0; i< num_edge; i++){
      if(tableT[i][current_truss-3]!=0){
          temp_table[i] =-1;
          size_maxTruss++;
      }
  }
 
  if (size_maxTruss!=0){
    num_trussNew.push_back(size_maxTruss);
  }
  // check the number of edges for remains 
  for(int j = current_truss-3-1; j>=0 ; j--){
      int counter = 0;
    for( long long i =0; i< num_edge; i++){
      if(temp_table[i] ==-1) continue;
      if(tableT[i][j]!=0){
        temp_table[i] =-1;
        counter++;      
      }
    }
    size_maxTruss+= counter;
    num_trussNew.push_back(size_maxTruss);
  }
  delete[] temp_table;

    //sort in reverse order (truss 2 to max_truss)
  sort(num_trussNew.begin(), num_trussNew.end());

  reverse(num_trussNew.begin(), num_trussNew.end());
}

void ComputeSum(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               vector<int> &triangle_edge, int **tableT, 
                               int k,int max_truss, 
                              unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> &neighborList, 
                              edge_t &drop_edge){

  for(auto &it: triangle_edge){
    double current_sum = 0.0; 
    int support = 0;
   // int truss = max_truss;
    vector<edge_t> edgeList; // neighbor list 
    unordered_set<Pair, boost::hash<Pair>> neighbors_set; //pair of edges forming a triangle with key edge

   // supportEdge.insert ({ it, support });
     double max_re = 0.0;
    findNeighborsCurrentTrussSET(g, s, neighbors_set, it, tableT, k, edgeList);
    neighborList.insert(make_pair(it,neighbors_set));
    support = neighbors_set.size();
    for(auto &element :edgeList){
        int denominator = 0; 
        int counter = k;
        while(counter<= max_truss){
          denominator += tableT[element][counter-3];
          counter++;
        }
     current_sum += (double)1/ max((int)denominator-k+2,1);
    }
   current_sum = current_sum * support;

     if(current_sum >max_re){
        max_re = current_sum;
        drop_edge = it;
    }
  }

  return;
}

edge_t findEdgeDeleteSaveSmallV2(Graph *__restrict__  g,
                               TrussDecompositionState *__restrict__ s,
                               vector<int> &KTruss, int max_truss, int ** tableT, int k){
  for (int i = 0; i< (int)(KTruss.size());i++){
    edge_t e_prime= KTruss[i];  //edge e'
    edge_t e = e_prime;
    int count =  tableT[e_prime][max_truss-3];
    if(count==0) continue;    
    if(count + 2 == max_truss){ 
      vector <edge_t> all_neighbor; // store all the edges which forming a triangle with e' 
      vector <edge_t> neighbor;
      findEdgeFormTriangle(g, s, all_neighbor, e_prime);
      double max_ratio = 0.0;
      //find edge e where it forming a triangle with e_prime and it trussness =  max_truss
      for(auto & it : all_neighbor){
        if(tableT[it][max_truss-3] >=max_truss-2 ){
          int counter_graterK = 0;
          int counter_smallerK = 0;
          for(int j=0; j<max_truss-2; j++){
            if(j<k-3){
              counter_smallerK+=tableT[it][j];
              }
              else{
                counter_graterK+=tableT[it][j];
              }
          }
          double current_ratio = 0;
          if(counter_smallerK!=0)
            current_ratio = (double)counter_graterK/counter_smallerK;
          else 
            current_ratio = counter_graterK;
          if(max_ratio < current_ratio ){
            max_ratio = current_ratio;
            e = it;
          }
        }
      }
      return e;
    }
  }
  cout<<"Can't find any edges to delete"<<endl;
  return -1;
}

void dropManyEdge(Graph *__restrict__ g,
               TrussDecompositionState *__restrict__ s, vector<edge_t> &edgeSet){
  auto mark_node = [&](node_t node){
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };
 
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };
  for(auto it : edgeSet){
      s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = it;
  }
  
     while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }

    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

}

// compute the size of each truss and store into num_truss
void ComputeTrussNumberCopy(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               Graph *__restrict__ g_receive,
                               TrussDecompositionState *__restrict__ s_receive,
                                vector<size_t> &num_truss , edge_t num_runs) {
  // Initialize data structures.
  edge_t number_edge = g->adj_start.back();
  edge_t dropped_edges = 0;
  node_t first_not_empty = 0;
  edge_t edge_count = 0;
  s->to_drop_start = 0;
  s->to_drop_end = 0;

  // count how many edges left
    for (node_t i = 0; i < g->N(); i++) {
      if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
      thread_local std::vector<node_t> local_counts;
       local_counts.clear();
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      local_counts.resize(s->adj_list_end[i] - s->adj_list_fwd_start[i]);
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edge_count++;
       }
    }
    s->edges_to_drop.resize(number_edge);
  // Mark a node for recomputing triangle counts after removing one of its
  // edges.
  auto mark_node = [&](node_t node) {
    if (!s->dirty_nodes_bitmap[node]) {
      s->dirty_nodes.push_back(node);
    }
    s->dirty_nodes_bitmap[node] = true;
  };

  number_edge /= 2;
  s->edge_deleted.resize((number_edge + 31) >> 5);
  s->min_size =2;

  // Remove an edge, marking extremes and decreasing their current degrees.
  auto drop_edge = [&](edge_t e) {
    s->actual_degree[s->edge1[e]]--;
    s->actual_degree[s->edge2[e]]--;
    mark_node(s->edge1[e]);
    mark_node(s->edge2[e]);
  };

  edge_t iter_num = 0;
  
  while (dropped_edges != edge_count) {
    edge_t iters = 0;
    iter_num++;
  
   if(iter_num == num_runs){
    copyGraph(g_receive, g);
    copyTruss(g, s_receive, s);
   }
    // Remove all the edges marked for deletion and update triangle counts until
    // no edge is removed.
    while (s->to_drop_start != s->to_drop_end) {
      edge_t drop_end = s->to_drop_end;
      iters++;
      for (edge_t i = s->to_drop_start; i < drop_end; i++) {
        edge_t e = s->edges_to_drop[i];
        s->edge_deleted[e >> 5] |= 1U << (e & 0x1F);
        dropped_edges++;
        drop_edge(e);
      }
      s->iter_drop_start = s->to_drop_start;
      s->to_drop_start = s->to_drop_end;
      UpdateTriangleCounts(g, s);
    }
    if((edge_count - dropped_edges) !=0){
      num_truss.push_back(edge_count - dropped_edges);
    }

    s->min_size++;
    
    s->to_drop_start = s->to_drop_end = s->iter_drop_start = 0;

    // Ignore (now and in future iterations) nodes at the beginning of the graph
    // with degree 0. As the graph is in degeneracy order, nodes at the
    // beginning are much more likely to have degree 0 than other nodes.
    while (first_not_empty < g->N() &&
           g->adj_start[first_not_empty] == s->adj_list_end[first_not_empty]) {
      first_not_empty++;
    }

    edge_t min_triangle_count = g->N();
    
#pragma omp parallel for schedule(guided) reduction(min : min_triangle_count)
    for (node_t i = first_not_empty; i < g->N(); i++) {
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
           cnt++) {
        auto edg = s->edge_id[cnt];
        size_t count =
            s->edge_triangles_count[edg].load(std::memory_order_relaxed);
        if (min_triangle_count > count) min_triangle_count = count;
        if (count + 2 < s->min_size) {
          s->edges_to_drop[s->to_drop_end.fetch_add(
              1, std::memory_order_relaxed)] = edg;
        }
      }
    }
   FullFence();
    if (min_triangle_count != g->N() && min_triangle_count + 2 >= s->min_size) {
      size_t  diff = min_triangle_count + 2 -s->min_size;
         while(diff>0){
           num_truss.push_back(edge_count - dropped_edges);
           diff --;
       }           
      s->min_size = min_triangle_count + 2;
    }
  }
}

void UpdateSumNeighSET(TrussDecompositionState *__restrict__ s, int k, 
                              unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> &neighborList){
  //remove the edge in map_sum
  unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> ::iterator it2;

  vector<int> removeEdges;  //edges trusness<k
  //edges forming a triangle with edge in removeEdges 
  unordered_set<edge_t> updateSum; 

  //find edge turssness <k
  for(it2 = neighborList.begin(); it2 != neighborList.end(); ++it2){
    if(s->trussness[it2->first] < k){
      removeEdges.push_back(it2->first);
    }
  }
 for(auto &del_edg: removeEdges){
     it2 = neighborList.find(del_edg);
     for(auto &pair_edg: it2->second){
        unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> ::iterator find_neigh;
        pair <edge_t,edge_t> new_pair;
        edge_t key_edge; 
         
        key_edge = pair_edg.second;
        if(del_edg < pair_edg.first){
          new_pair = make_pair(del_edg, pair_edg.first);
        }
        else{
          new_pair = make_pair(pair_edg.first, del_edg);
        }
         
        find_neigh = neighborList.find(key_edge);
        find_neigh->second.erase(new_pair);
  
        key_edge = pair_edg.first;
        if(del_edg < pair_edg.second){
            new_pair = make_pair(del_edg, pair_edg.second);
        }
        else{
          new_pair = make_pair(pair_edg.second, del_edg);
        }   
        find_neigh = neighborList.find(key_edge);
        find_neigh->second.erase(new_pair);
     }
     neighborList.erase(it2);
 }

}


edge_t updateSUM(unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> &neighborList, int k){
            
  unordered_map<int, unordered_set<Pair, boost::hash<Pair>>>::iterator it2;

   edge_t drop_edge;
   double max_re = 0.0;
   for(auto &it: neighborList){
    double re = 0;
   // int sup = 0;
    for(auto &each_pair: it.second){
        it2 = neighborList.find(each_pair.first);

        re+=(double) 1/max((int)it2->second.size()-k,1);
        it2 = neighborList.find(each_pair.second);

         re+=(double) 1/max((int)it2->second.size()-k,1);
    }

    re = re*it.second.size();
     if(re >max_re){
        max_re = re;
        drop_edge = it.first;
    }
  }
  return drop_edge;
}

void UpdateSumNeigh(TrussDecompositionState *__restrict__ s,
                              int k, unordered_map<int,double> &sum, 
                              unordered_map<int, vector<edge_t>> &neighborList, 
                              unordered_map<int, int> &supportEdge){
  //cout<<"*******UpdateSumNeigh***** k ="<<k<<endl;  
  //remove the edge in map_sum
  unordered_map<int,double>::iterator it1;
  unordered_map<int, vector<edge_t>> ::iterator it2;
  unordered_map<int,int>::iterator it3;

  vector<int> removeEdges;  //edges trusness<k
  // edges forming a triangle with edge in removeEdges 
  unordered_set<edge_t> updateSum; 

  //find edge turssness <k
 for(it1 = sum.begin(); it1 != sum.end(); ++it1){
    if(s->trussness[it1->first] < k){
    //cout<<"trussness smaller than k, edge = "<<it1->first<<" , trussness = "<<s->trussness[it1->first] <<endl;
      removeEdges.push_back(it1->first);
      it3 = supportEdge.find(it1->first);
      supportEdge.erase(it3);
    }
 }
 //find edges forming a triangle with edge in removeEdges 
 for(auto &it: removeEdges){
     //it -> the edge has trusness <k
     sum.erase(it);
     it2 = neighborList.find(it);
     //find the neighbor list and insert distinct edges
     for(auto &nei :it2->second){
        updateSum.insert(nei);
     }
 }

// clean the updateEdge, remove the edge trusness<k
 for(auto &it: removeEdges){
     updateSum.erase(it); 
 }

 //update neighborList: delete edges trusness <k;
for(auto &target: removeEdges){
 for(auto &it: updateSum){
      it2 = neighborList.find(it);
      vector<edge_t> temp;
      for (unsigned i=0; i<it2->second.size(); ++i){
        if(it2->second[i] != target){
           temp.push_back(it2->second[i]);
        }
        else{
          if(i%2 ==0){
            i++;
          }
          else{
            temp.pop_back();
          }
        }
      }
      it2->second = temp;
       //update support
      int support = temp.size();
      support /=2;
      it3 = supportEdge.find(it);
      it3->second = support;
  }
}

//remove edges that trusness<k in neighborList
for(auto &it: removeEdges){ 
      it2 = neighborList.find(it);
      neighborList.erase(it2);
 }
//update sum
  for(auto &it: neighborList){
    double re = 0;
    int sup = 0;
      for (unsigned i = 0; i<it.second.size(); ++i){
         it3 = supportEdge.find(it.second[i]);
         sup = it3->second;
         if((sup-k+2) ==0){
          re+=1;
         }
         else{
          re+= (double)1/(sup-k+2);
         }
      }
      it3 = supportEdge.find(it.first);
      sup = it3->second;
      
      re = re*sup;
      it1 = sum.find(it.first);
      
      it1->second = re ;
  }
}
 

void ATK(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               Graph *__restrict__ g_max,
                               TrussDecompositionState *__restrict__ s_max,int k ) {
  vector<edge_t> del_edges_total;
 cout<<"------------------ ATk --------------------"<<endl;
  size_t num_edge = s->edge_id.size();
  size_t time_counter = 0;
  size_t pre_truss = s_max->min_size -1;
  size_t current_truss = 0;
  
  node_t first_not_empty = 0;  
  num_edge = num_edge/2;
  
  size_t edgeLeft = 0;
    // count how many edges left
  for (node_t i = 0; i < g->N(); i++) {
    if (s->adj_list_end[i] == s->adj_list_fwd_start[i]) continue;
    if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
         cnt++){
           edgeLeft++;
       }
    }
  
  while(pre_truss >= k){   
    vector<edge_t> deleted_edges;
     vector<size_t> num_trussNew;
    auto start = std::chrono::high_resolution_clock::now();
    first_not_empty = 0;  

    while (first_not_empty < g_max->N() &&
           g_max->adj_start[first_not_empty] == s_max->adj_list_end[first_not_empty]) {
      first_not_empty++;
    } 

    for (node_t i = first_not_empty; i < g_max->N(); i++) {
      for (edge_t cnt = s_max->adj_list_fwd_start[i]; cnt < s_max->adj_list_end[i];
           cnt++) {
        auto edg = s_max->edge_id[cnt];
        deleted_edges.push_back(edg);
        del_edges_total.push_back(edg);
      }
    }
    dropManyEdge(g,s,deleted_edges); // drop edges 
    if(edgeLeft != del_edges_total.size() ){
      Graph g_currt,  g_copy2;  // save current graph
      TrussDecompositionState s_currt,s_copy2 ;
      
      copyGraph(&g_currt, g);  
      copyTruss(g, &s_currt, s);
      
      copyGraph(&g_copy2, g);  
      copyTruss(g, &s_copy2, s);
      edge_t runs = 0;
      runs = ComputeTrussNumber(&g_copy2, &s_copy2);
      ComputeTrussNumberBase(&g_currt, &s_currt, g_max,s_max, num_trussNew, runs);
      current_truss = s_max->min_size-1;
    }
    else {
        current_truss = s->min_size;
    }
    auto end = std::chrono::high_resolution_clock::now();
    time_counter += std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
 
    pre_truss = current_truss;
  }
    cout<<"ATk Runtime = "<<time_counter<<" ms"<<endl;
    cout<<"ATk #. delete edge = "<<del_edges_total.size()<<endl;

}

void GTK(Graph *__restrict__ g,
                    TrussDecompositionState *__restrict__ s, 
                    Graph *__restrict__ g_max,
                    TrussDecompositionState *__restrict__ s_max,
                    size_t k, int max_truss){
  cout<<"------------------ GTk --------------------"<<endl;

  vector<edge_t> droped_edges;  // the edge deleted by heuristic
  vector<size_t> num_trussNew;  // number of edges in  each truss (updated graph)

  int current_truss = max_truss;
  long long num_edge = s->edge_id.size();
  num_edge = num_edge/2;

  size_t time_counter = 0;

  vector<int> KTruss;  // all edges at max_truss
   for (node_t i = 0; i < g_max->N(); i++) {
      if (s_max->adj_list_end[i] == s_max->adj_list_fwd_start[i]) continue;
      if(s_max->adj_list_end[i] <=s_max->adj_list_fwd_start[i]) continue;
       for (edge_t cnt = s_max->adj_list_fwd_start[i]; cnt < s_max->adj_list_end[i];
         cnt++){
          auto edg = s_max->edge_id[cnt];
          KTruss.push_back(edg);
       }
    }

    auto start = std::chrono::high_resolution_clock::now();
    while(max_truss >= k){
     
      edge_t e = KTruss[rand() % KTruss.size()];

      dropEdge(g, s, e);
      droped_edges.push_back(e);
  
      Graph g_currt,  g_copy2;  // save current graph
      TrussDecompositionState s_currt,s_copy2 ;
        
      copyGraph(&g_currt, g);  
      copyTruss(g, &s_currt, s);
        
      copyGraph(&g_copy2, g);  
      copyTruss(g, &s_copy2, s);
      edge_t runs = 0;
      runs = ComputeTrussNumber(&g_copy2, &s_copy2);
      num_trussNew.clear();
      ComputeTrussNumberCopy(&g_currt, &s_currt, g_max,s_max, num_trussNew, runs);
      KTruss.clear();
      current_truss = 0;
      // count the current max_truss (current_truss) and the set of edges are in this truss (KTruss)
      for (node_t i = 0; i < g_max->N(); i++) {
        if (s_max->adj_list_end[i] == s_max->adj_list_fwd_start[i]) continue;
        if(s_max->adj_list_end[i] <=s_max->adj_list_fwd_start[i]) continue;
         for (edge_t cnt = s_max->adj_list_fwd_start[i]; cnt < s_max->adj_list_end[i];
           cnt++){
            auto edg = s_max->edge_id[cnt];
            KTruss.push_back(edg);
         }
      }
      current_truss = s_max->min_size-1;
      max_truss = current_truss;
  }
  auto end = std::chrono::high_resolution_clock::now();
  time_counter = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
  cout<<"GTk Runtime = "<<time_counter<<" ms"<<endl;
  cout<<"GTk #. delete edge = "<<droped_edges.size()<<endl;
}

void MBHS(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               int k, int **tableT, int max_truss) {
   cout<<"------------------ MBH_s --------------------"<<endl;
  vector<edge_t> droped_edges;  // the edge deleted by heuristic

  int current_truss = 0;
  long long num_edge = s->edge_id.size();
  size_t time_counter = 0;

  num_edge = num_edge/2;

  vector<int> KTruss;  // all edges at max_truss
  for(int j = max_truss-3; j>=0 ; j--){
    for(long long i =0; i< num_edge; i++){
      if(tableT[i][j]>=j+1){
        current_truss = j+3; 
        KTruss.push_back(i);   
      }
    }
    if(current_truss!=0)
      break;
  }
  auto start = std::chrono::high_resolution_clock::now();
    while(max_truss >= k){
      edge_t e = findEdgeDelete(g,s,KTruss,max_truss,tableT);
      droped_edges.push_back(e);
      
      UpdateTruss(g, s, e, max_truss, tableT); //update table T
      KTruss.clear();
      current_truss = 2;
      // count the current max_truss (current_truss) and the set of edges are in this truss (KTruss)
      for(int j = max_truss-3; j>=0 ; j--){
        for(long long i =0; i< num_edge; i++){
            if(tableT[i][j]>=j+1){
              current_truss = j+3; 
              KTruss.push_back(i);                 
            }
          }
          if(current_truss>2)
              break;
      }
      max_truss = current_truss;
    }
    auto end = std::chrono::high_resolution_clock::now();
    time_counter = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    cout<<"MBH_s Runtime = "<<time_counter<<" ms"<<endl;
    cout<<"MBH_s #. delete edge = "<<droped_edges.size()<<endl;
}

void MBHC(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               int k,  int **tableT, int max_truss) {
   cout<<"------------------ MBH_c --------------------"<<endl;
  vector<edge_t> droped_edges;  // the edge deleted by heuristic
 
  int current_truss = max_truss;
  long long num_edge = s->edge_id.size();
  size_t time_counter = 0;
  num_edge = num_edge/2;

   vector<int> KTruss;  // all edges at max_truss
    for(long long i =0; i< num_edge; i++){
      if(tableT[i][ max_truss-3]>=max_truss-2){
        current_truss = max_truss; 
        KTruss.push_back(i);   
      }
    }
 auto start = std::chrono::high_resolution_clock::now();
  
  while(current_truss >= k){
      edge_t e = findEdgeDeleteSaveSmallV2(g,s,KTruss,current_truss,tableT,k);
      droped_edges.push_back(e);
      UpdateTruss(g,s, e, max_truss, tableT); //update table T
      
      KTruss.clear();
      current_truss = 2;
      // count the current max_truss (current_truss) and the set of edges are in this truss (KTruss)
      for(int j = max_truss-3; j>=0 ; j--){
        for(long long i =0; i< num_edge; i++){
          if(tableT[i][j]>=j+1){
            current_truss = j+3; 
            KTruss.push_back(i);                 
          }
        }
        if(current_truss>2)
          break;
      }
    }
     auto end = std::chrono::high_resolution_clock::now();
      time_counter = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
   
   cout<<"MBH_c Runtime = "<<time_counter<<" ms"<<endl;
  cout<<"MBH_c #. delete edge = "<<droped_edges.size()<<endl;

}

void SNH(Graph *__restrict__ g,
                               TrussDecompositionState *__restrict__ s, 
                               int k, int **tableT, int max_truss ){
  cout<<"------------------ SNH --------------------"<<endl;
  vector<edge_t> droped_edges;  // the edge deleted by heuristic

  int current_truss = max_truss;
  long long num_edge = s->edge_id.size()/2;

  size_t time_counter = 0;

   vector<int> KTruss;  // all edges with truss>=k
    for (node_t i = 0; i < g->N(); i++) {
      if(s->adj_list_end[i] <=s->adj_list_fwd_start[i]) continue;
      for (edge_t cnt = s->adj_list_fwd_start[i]; cnt < s->adj_list_end[i];
        cnt++){
          auto edg = s->edge_id[cnt];
          int truss =s->trussness[edg];
          if(truss >= k){
              KTruss.push_back(edg);
          }
      }
    }
    edge_t dropEdge;
    auto start = std::chrono::high_resolution_clock::now();
    unordered_map<int, unordered_set<Pair, boost::hash<Pair>>> neighborList_set;//<key, piar of edges forming a triangle with key edge >
    ComputeSum(g,s, KTruss, tableT,  k, current_truss, neighborList_set, dropEdge);
    
    //cout<<"drop edge = "<<dropEdge<<endl;
   while(current_truss >=k){
      droped_edges.push_back(dropEdge);
      UpdateTruss(g,s,dropEdge, max_truss, tableT); //update table T
      current_truss = 2;
      //count the current max_truss (current_truss) and the set of edges are in this truss (KTruss)
       for(int j = max_truss-3; j>=0 ; j--){
          for(long long i =0; i< num_edge; i++){
              if(tableT[i][j]>=j+1){
                current_truss = j+3; 
                break;               
              }
          }
          if(current_truss>2)
              break;
      }
      if(current_truss>=k){
          UpdateSumNeighSET(s, k, neighborList_set);
          dropEdge = updateSUM(neighborList_set , k-2);
      }
    }
    auto end = std::chrono::high_resolution_clock::now();
    time_counter = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
  
  cout<<"SNH Runtime = "<<time_counter<<" ms"<<endl;
  cout<<"SNH #. delete edge = "<<droped_edges.size()<<endl;
}

void Run(int argc, char **argv) {
  Graph g;
  g.adj_start.init(FileFromStorageDir(FLAGS_storage_dir, "adj_start.bin"));
  g.adj.init(FileFromStorageDir(FLAGS_storage_dir, "edges.bin"));
  
  std::chrono::high_resolution_clock::time_point reading;
  g.Read(FLAGS_filename, ParseGraphFormat(FLAGS_format),
         GraphPermutation::kDegeneracyOrder,
         /*forward_only=*/false, &reading);

  TrussDecompositionState s;
  s.SetStorageDir(FLAGS_storage_dir);

  int k = atoi(argv[1]);

  edge_t runs = 0; // how many runs are needed for computing max truss
  graphInit(&g, &s); // INIT graph 
 
  long long  edges = s.edge_id.size();
  edges /= 2;

  Graph g_copy, g_copy2;  // save original graph for counting runs
  TrussDecompositionState s_copy, s_copy2;

  copyGraph(&g_copy2, &g);  
  copyTruss(&g, &s_copy2, &s);
  
  runs = ComputeTrussNumber(&g_copy2, &s_copy2); // to get the max truss of g and runs
  
  int max_truss =  s_copy2.min_size-2;  //max truss to set the size of 2d table T
 
  cout<<"Number of edges = "<<edges<<" , max_truss = "<<max_truss<<endl;

 int **tableT = AllocateDynamicArray(edges,max_truss-2);
 int **tableT_ss = AllocateDynamicArray(edges,max_truss-2);
 int **tableT_neighs = AllocateDynamicArray(edges,max_truss-2);
 for(long long  i = 0;i<edges;i++){
  for(int j = 0;j<max_truss-2;j++){
      tableT[i][j] = 0;
      tableT_ss[i][j] = 0;
      tableT_neighs[i][j] = 0;
  }
 }
  vector<size_t> num_truss;  // stock for number of edges in each trussness 
  
  Graph g_max;  // max truss stock
  TrussDecompositionState s_max;
  
  copyGraph(&g_copy, &g);  
  copyTruss(&g, &s_copy, &s);
  //compute table  T (tableT); max_truss graph (g_max); update the trussness of each edge in s;
  ComputeTable(&s,&g_copy, &s_copy, max_truss, tableT, &g_max,&s_max, num_truss, runs); 


  for(long long  i = 0;i<edges;i++){
    for(int j = 0;j<max_truss-2;j++){
        tableT_ss[i][j] = tableT[i][j];
        tableT_neighs[i][j] =  tableT[i][j];
    }
 }

  Graph g_V1, g_V2, g_h1, g_ss,  g_max2, g_neighs;  // copy graphs for  Variation 1 and 2
  TrussDecompositionState s_V1, s_V2, s_h1,s_ss,  s_max2, s_neighs;
  copyGraph(&g_V1, &g);  
  copyTruss(&g, &s_V1, &s);
  
  copyGraph(&g_V2, &g);  
  copyTruss(&g, &s_V2, &s);
  
  copyGraph(&g_h1, &g);  
  copyTruss(&g, &s_h1, &s);

  copyGraph(&g_ss, &g);  
  copyTruss(&g, &s_ss, &s);
  
  copyGraph(&g_neighs, &g);  
  copyTruss(&g, &s_neighs, &s);

  copyGraph(&g_max2, &g_max);  
  copyTruss(&g_max, &s_max2, &s_max);

  
  ATK(&g_V1, &s_V1, &g_max,&s_max, k);
  GTK(&g_V2,&s_V2, &g_max2, &s_max2, k ,max_truss);

  MBHS(&g_h1, &s_h1,  k , tableT,max_truss);
  FreeDynamicArray(tableT,edges);
  
  MBHC(&g_ss, &s_ss,  k , tableT_ss,max_truss);
  FreeDynamicArray(tableT_ss,edges);
  
  SNH(&g_neighs, &s_neighs,  k , tableT_neighs,max_truss);
  FreeDynamicArray(tableT_neighs,edges);

}

int main(int argc, char **argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  Run(argc, argv);
}
